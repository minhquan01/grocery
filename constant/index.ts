export const secretKey = process.env.secretKey ?? "";

export const authToken = {
  accessToken: "accessToken",
  refreshToken: "refreshToken",
};

export const socketUrl = "socket/messages";
