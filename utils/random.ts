import { listColorTag } from "@/lib/mock.main";

export const getRandomColor = () => {
  const randomIndex = Math.floor(Math.random() * listColorTag.length);
  return listColorTag[randomIndex];
};
