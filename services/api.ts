import { LoginProps, newAccount } from "@/interfaces";
import axios from "axios";
import { axiosCustom } from "./axios.cutom";
import toast from "react-hot-toast";

export const authApi = {
  loginApi: async (values: LoginProps) => {
    try {
      const response = await axios.post("/api/auth/login", values);
      return response;
    } catch (error) {
      toast.error((error as any).response.data.message);
      throw error;
    }
  },
  fetchMe: async () => {
    try {
      const response = await axiosCustom.get("/auth/me");
      return response.data;
    } catch (error) {
      throw error;
    }
  },
  registerApi: async (values: newAccount) => {
    try {
      const res = await axios.post("/api/auth/register", values);
      return res;
    } catch (error) {
      throw error;
    }
  },
};
