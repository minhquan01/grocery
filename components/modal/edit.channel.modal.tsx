"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { useModal } from "@/hooks/use.modal.store";
import { zodResolver } from "@hookform/resolvers/zod";
import { useParams, useRouter } from "next/navigation";

import { ChannelType } from "@prisma/client";
import ButtonCustom from "../button.custom";
import RenderInputFrom from "../form/render.input";
import { InitModal } from "./init.modal";
import SelectCustom from "../select.custom";
import { useEffect, useMemo } from "react";
import { createChannel, editChannel } from "@/services/chat";

const formSchema = z.object({
  name: z.string().min(1, {
    message: "Channel name is required.",
  }),
  type: z.nativeEnum(ChannelType),
});
const EditChannelModal = () => {
  const { isOpen, onClose, type, data } = useModal();
  const router = useRouter();
  const params = useParams();

  const isModalOpen = isOpen && type === "editChannel";

  const { channel, server } = data;

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      type: ChannelType.TEXT || channel?.type,
    },
  });

  useEffect(() => {
    if (channel) {
      form.setValue("name", channel.name);
      form.setValue("type", channel.type as any);
    }
  }, [form, channel]);

  const selectTypeChannel = useMemo(() => {
    return Object.values(ChannelType).map((type) => ({
      value: type,
      name: type.toLowerCase(),
    }));
  }, [ChannelType]);

  const isLoading = form.formState.isSubmitting;

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      if (!channel?.id) return;
      const query = {
        serverId: String(params?.serverId),
      };
      const res = await editChannel(channel?.id, query, values);
      if (res.status === 200) {
        form.reset();
        onClose();
        router.refresh();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    form.reset();
    onClose();
  };

  return (
    <InitModal
      action={false}
      title="Sửa kênh"
      isModalOpen={isModalOpen}
      onClose={handleClose}
    >
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <div className="space-y-8 ">
            <RenderInputFrom form={form} label="Tên kênh" name="name" />
            <div className="w-full">
              <FormField
                control={form.control}
                name="type"
                render={({ field }) => {
                  return (
                    <FormItem>
                      <FormLabel className="text-base text-[#FFFFFFCC]">
                        Loại kênh
                      </FormLabel>
                      <SelectCustom
                        placeholder="Chọn kênh"
                        dataSelect={selectTypeChannel}
                        disabled={isLoading}
                        onValueChange={field.onChange}
                        defaultValue={field.value}
                      />
                    </FormItem>
                  );
                }}
              />
            </div>
          </div>
          <div className="flex items-center justify-between">
            <ButtonCustom
              className="w-full"
              isLoading={isLoading}
              type="submit"
            >
              Lưu
            </ButtonCustom>
          </div>
        </form>
      </Form>
    </InitModal>
  );
};

export default EditChannelModal;
