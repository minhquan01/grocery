import ServerSidebar from "@/containers/chat/server.sidebar";
import { checkAuthToken } from "@/lib/auth";
import { db } from "@/lib/prisma";
import { redirect } from "next/navigation";
import React, { ReactNode } from "react";

const ServerDetailLayout = async ({
  children,
  params,
}: {
  children: ReactNode;
  params: { serverId: string };
}) => {
  const profile: any = checkAuthToken();

  if (!profile) {
    return redirect("/");
  }

  const server = await db.server.findUnique({
    where: {
      id: params.serverId,
      members: {
        some: {
          profileId: profile.id,
        },
      },
    },
  });

  if (!server) {
    return redirect("/");
  }

  return (
    <div className="h-full">
      <div className="max-md:hidden flex h-full w-60 z-20 fixed flex-col inset-y-0">
        <ServerSidebar serverId={params.serverId} />
      </div>
      <p className="h-full md:pl-60">{children}</p>
    </div>
  );
};

export default ServerDetailLayout;
