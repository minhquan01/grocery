import React, { ReactNode } from "react";

import {
  CommandDialog,
  CommandEmpty,
  CommandInput,
  CommandList,
} from "@/components/ui/command";

interface CommandDialogCustomProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  placeholder: string;
  children: ReactNode;
}

const CommandDialogCustom = ({
  open,
  setOpen,
  placeholder,
  children,
}: CommandDialogCustomProps) => {
  return (
    <CommandDialog open={open} onOpenChange={setOpen}>
      <CommandInput placeholder={placeholder} />
      <CommandList>
        <CommandEmpty>Không có kết quả</CommandEmpty>
        {children}
      </CommandList>
    </CommandDialog>
  );
};

export default CommandDialogCustom;
