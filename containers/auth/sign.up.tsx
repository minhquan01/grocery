"use client";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import React from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import RenderInputFrom from "@/components/form/render.input";
import Link from "next/link";
import axios from "axios";
import { Loader2 } from "lucide-react";
import { useRouter } from "next/navigation";
import ButtonCustom from "@/components/button.custom";
import { useAuth } from "@/components/providers/auth.provider";

const SignUpContent = () => {
  const { register } = useAuth();

  const formSchema = z.object({
    name: z.string().min(1, {
      message: "Hãy cho tôi biết tên bạn",
    }),
    username: z.string().min(3, {
      message: "Tên ít nhất 3 ký tự",
    }),
    password: z.string().min(3, {
      message: "Mật khẩu quá ngắn",
    }),
  });

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      username: "",
      password: "",
    },
  });
  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    await register(values);
  };

  const isLoading = form.formState.isSubmitting;

  return (
    <div className="md:min-w-[500px]">
      <div className="mb-5 text-center">
        <h1 className="text-lg md:text-2xl font-medium">
          Chào mừng bạn đến với Pure 🥺
        </h1>
        <p className="text-[13px] md:text-sm mt-2 dark:text-white/70 text-black/70">
          Tạo tài khoản mới thui nàoo
        </p>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-5">
          <div className="space-y-4">
            {fieldLogin.map((item, idx) => (
              <RenderInputFrom
                form={form}
                label={item.label}
                name={item.name}
                isLoading={isLoading}
                type={item.type}
                key={idx}
              />
            ))}
          </div>

          <ButtonCustom isLoading={isLoading} type="submit" className="w-full">
            Đăng ký
          </ButtonCustom>
          <div className="text-[15px] dark:text-white/60 text-black/60">
            Bạn đã có tài khoản?{" "}
            <Link
              className="underline text-primary/80 hover:text-primary"
              href="/sign-in"
            >
              Đăng nhập
            </Link>
          </div>
        </form>
      </Form>
    </div>
  );
};

export default SignUpContent;

const fieldLogin = [
  {
    name: "name",
    label: "Tên của bạn",
  },
  {
    name: "username",
    label: "Tên đăng nhập",
  },
  {
    name: "password",
    label: "Mật khẩu",
    type: "password",
  },
];
