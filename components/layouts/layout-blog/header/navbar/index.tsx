import Link from "next/link";
import React from "react";
import { listNav } from "./mock";
import { usePathname, useRouter } from "next/navigation";

const NavbarBlog = () => {
  const pathname = usePathname();
  const router = useRouter();
  return (
    <div className="hidden md:flex gap-x-2 items-center">
      {listNav.map((item, idx) => (
        <Link
          href={item.url}
          className={`px-[18px] py-2.5 rounded-full ${
            pathname === item.url
              ? "bg-backgroundHover dark:bg-dark-backgroundHover"
              : "hover:bg-backgroundHover/50 dark:hover:bg-dark-backgroundHover/50"
          }`}
          key={idx}
        >
          {item.name}
        </Link>
      ))}
    </div>
  );
};

export default NavbarBlog;
