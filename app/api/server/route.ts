import { checkAuthToken } from "@/lib/auth";
import { db } from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
import { v4 as uuidv4 } from "uuid";
import { headers } from "next/headers";
import { NextResponse } from "next/server";
import { RoleAccount } from "@prisma/client";
import { ServerMessage } from "../message";

export async function POST(req: Request) {
  try {
    const { name, avatar } = await req.json();
    const profile: any = await checkAuthToken();
    if (!profile) {
      return NextResponse.json(
        {
          message: "Unauthorized",
        },
        { status: 401 }
      );
    }

    const server = await db.server.create({
      data: {
        profileId: profile?.id,
        name,
        avatar,
        inviteCode: uuidv4(),
        channels: {
          create: [
            {
              name: "general",
              profileId: profile?.id,
            },
          ],
        },
        members: {
          create: [
            {
              profileId: profile.id,
              role: RoleAccount.ADMIN,
            },
          ],
        },
      },
    });

    return NextResponse.json(
      {
        message: ServerMessage.SERVER_CREATE_SUCCESS,
        server,
      },
      { status: 200 }
    );
  } catch (error) {
    return new NextResponse("Internal Error", { status: 500 });
  }
}

export async function GET() {
  try {
    const profile = await checkAuthToken();

    if (!profile) {
      return NextResponse.json(
        {
          message: "Unauthorized",
        },
        { status: 401 }
      );
    }
    const servers = await db.server.findMany({
      where: {
        members: {
          some: {
            profileId: profile.id,
          },
        },
      },
    });
    return NextResponse.json(servers);
  } catch (error) {
    return new NextResponse("Internal Error", { status: 500 });
  }
}
