export const getErrorMessage = (error: any) => {
  let message: string[] | string;

  message =
    error?.response?.data?.message ?? error?.message ?? "Có lỗi gì đó rùi";
  if (typeof message === "string") {
    return message;
  } else {
    return message[0];
  }
};
