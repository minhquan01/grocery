import { Hash } from "lucide-react";
import React from "react";

interface ChatWelcomeProps {
  type: "channel" | "conversation";
  name: string;
}

const ChatWelcome = ({ name, type }: ChatWelcomeProps) => {
  return (
    <div className="space-y-2 px-4 mb-4">
      {type === "channel" && (
        <div className="h-[75px] w-[75px] rounded-full bg-zinc-500 dark:bg-white/5 flex items-center justify-center">
          <Hash className="h-12 w-12" />
        </div>
      )}
      <p className="text-xl md:text-3xl font-bold">
        {type === "channel" ? "Chào mừng đến #" : ""}
        {name}
      </p>
      <p className="text-zinc-600 dark:to-zinc-400 text-sm">
        {type === "channel"
          ? `Bắt đầu chat ở kênh #${name}`
          : `Bắt đầu cuộc trò chuyện với ${name}`}
      </p>
    </div>
  );
};

export default ChatWelcome;
