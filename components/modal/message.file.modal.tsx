"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";

import { Form, FormControl, FormField, FormItem } from "@/components/ui/form";
import { useModal } from "@/hooks/use.modal.store";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter } from "next/navigation";

import { createServerChat } from "@/services/chat";
import ButtonCustom from "../button.custom";
import RenderInputFrom from "../form/render.input";
import UploadAvatar from "../upload/upload.avatar";
import { InitModal } from "./init.modal";
import UploadImage from "../upload.image";
import messageApi from "@/services/message";
const formSchema = z.object({
  fileUrl: z.string().min(1, {
    message: "File không hợp lệ.",
  }),
});
const MessageFileModal = () => {
  const { isOpen, onClose, type, data } = useModal();
  const router = useRouter();

  const isModalOpen = isOpen && type === "messageFile";

  const { apiUrl, query } = data;

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      fileUrl: "",
    },
  });

  const isLoading = form.formState.isSubmitting;

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    if (!apiUrl || !query) return;
    try {
      await messageApi.createMessage(
        apiUrl,
        { ...values, content: values.fileUrl },
        query
      );
      form.reset();
      router.refresh();
      onClose();
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    form.reset();
    onClose();
  };

  return (
    <InitModal
      action={false}
      title="Thêm đính kèm"
      description="Gửi một tập tin dưới dạng tin nhắn."
      isModalOpen={isModalOpen}
      onClose={handleClose}
    >
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <div className="space-y-8 ">
            <div className="flex items-center justify-center text-center">
              <FormField
                control={form.control}
                name="fileUrl"
                render={({ field }) => {
                  return (
                    <FormItem>
                      <FormControl>
                        <UploadImage
                          endpoint="messageFile"
                          value={field.value}
                          onChange={field.onChange}
                        />
                      </FormControl>
                    </FormItem>
                  );
                }}
              />
            </div>
          </div>
          <div className="flex items-center justify-between">
            <ButtonCustom
              className="w-full"
              isLoading={isLoading}
              type="submit"
            >
              Gửi
            </ButtonCustom>
          </div>
        </form>
      </Form>
    </InitModal>
  );
};

export default MessageFileModal;
