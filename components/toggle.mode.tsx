"use client";

import { useTheme } from "next-themes";
import React from "react";
import { IconMoon, IconSearch, IconSun } from "@/assets";

const ToggleMode = () => {
  const { setTheme } = useTheme();

  return (
    <div>
      <div
        onClick={() => setTheme("dark")}
        className="dark:hidden w-10 h-10 md:h-12 md:w-12 hover:bg-bgIconHover rounded-full dark:hover:bg-bgIconHoverDark flex items-center justify-center cursor-pointer select-none"
      >
        <IconSun />
      </div>
      <div
        onClick={() => setTheme("light")}
        className="hidden w-10 h-10 md:h-12 md:w-12 hover:bg-bgIconHover rounded-full dark:hover:bg-bgIconHoverDark dark:flex items-center justify-center cursor-pointer select-none"
      >
        <IconMoon />
      </div>
    </div>
  );
};

export default ToggleMode;
