"use client";

import ButtonCustom from "@/components/button.custom";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React from "react";

const mock = [
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-8-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },

  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
  {
    title: "Live with Adam Wathan at Rails World 2023",
    image:
      "https://res.cloudinary.com/daily-now/image/upload/f_auto,q_auto/v1/posts/1703c9034d21715c4ecfc0c6197a1e83?_a=AQAEufR",
    category: ["Front End", "Ruby"],
    tags: ["react", "webdev", "github"],
    publishAt: "2023-10-08 15:23:04",
    author: {
      avatar:
        "https://i.pinimg.com/564x/f9/50/0d/f9500d580442b6019b98526aa6cd185a.jpg",
      name: "minhquan",
    },
  },
];

const ListBlog = () => {
  const router = useRouter();
  return (
    <>
      <div className="mb-20">
        <div className="flex items-center justify-end">
          <ButtonCustom
            variant="outline"
            onClick={() => router.push(`/blog/new`)}
          >
            Tạo bài viết
          </ButtonCustom>
        </div>
      </div>
      <div className="grid grid-cols-4 gap-5">
        {mock.map((item, idx) => (
          <div
            key={idx}
            onClick={() => router.push(`/blog/${item.title}`)}
            className="rounded-2xl cursor-pointer border border-black/20 hover:border-black/30 dark:border-white/10 dark:hover:border-white/20 py-3 px-2 dark:bg-[#161E2B] hover:translate-y-[-2px] transition-all duration-100"
          >
            <h1 className="line-clamp-2 text-lg font-extrabold">
              {item.title}
            </h1>
            <img
              className="h-36 mt-3 rounded-lg w-full object-cover"
              src={item.image}
              alt={item.title}
            />
            <div className="mt-3">
              <div className="flex items-center gap-x-3">
                <img
                  className="w-6 h-6 rounded-full"
                  src={item.author.avatar}
                  alt={item.author.name}
                />
                <div className="flex items-center gap-x-1 text-xs opacity-70">
                  <span className="">{item.author.name}</span>
                  <p>·</p>
                  <span>{new Date(item.publishAt).toLocaleDateString()}</span>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default ListBlog;
