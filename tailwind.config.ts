/** @type {import('tailwindcss').Config} */
import { withUt } from "uploadthing/tw";

module.exports = withUt({
  darkMode: ["class"],
  content: [
    "./pages/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./containers/**/*.{ts,tsx}",
    "./assets/**/*.{ts,tsx}",
    "./app/**/*.{ts,tsx}",
    "./src/**/*.{ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        dark: {
          background: "#111926",
          content: "#0C121D",
          backgroundHover: "rgb(31,41,55)",
          subText: "#E5E7EB",
          box: "#1F2937",
        },
        content: "#F3F4F6",
        primary: "#1EB141",
        text: "#F9FAFB",
        subText: "#111827",
        backgroundHover: "rgb(243,244,246)",
        box: "#F3F4F6",
      },
      maxWidth: {
        mobile: "90%",
        main: "1280px",
      },
      width: {
        mobile: "90%",
        main: "1280px",
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
});
