import React from "react";
import { InitModal } from "./init.modal";
import { useModal } from "@/hooks/use.modal.store";
import { Search } from "lucide-react";
import { LiaSearchSolid } from "react-icons/lia";

const SearchModal = () => {
  const { isOpen, onClose, type } = useModal();
  const isModalOpen = isOpen && type === "search";

  return (
    <InitModal noBg action={false} isModalOpen={isModalOpen} onClose={onClose}>
      <div className="h-fit fixed inset-0 mx-auto -translate-y-80">
        <div className="flex items-center gap-x-2 py-5 px-4 bg-white dark:bg-[rgb(55,65,81)] shadow-lg rounded-xl w-[512px]">
          <LiaSearchSolid size={24} />
          <input
            type="text"
            className="flex-1 w-full h-full bg-transparent border-none outline-none"
            placeholder="Tìm bài viết"
          />
        </div>
      </div>
    </InitModal>
  );
};

export default SearchModal;
