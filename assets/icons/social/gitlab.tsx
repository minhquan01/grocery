import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill="none"
      viewBox="0 0 48 48"
    >
      <path fill="#e24329" d="M32 61.477L43.784 25.2H20.216z"></path>
      <path fill="#fc6d26" d="M32 61.477L20.216 25.2H3.7z"></path>
      <path
        fill="#fca326"
        d="M3.7 25.2L.12 36.23a2.44 2.44 0 00.886 2.728L32 61.477z"
      ></path>
      <path
        fill="#e24329"
        d="M3.7 25.2h16.515L13.118 3.366c-.365-1.124-1.955-1.124-2.32 0z"
      ></path>
      <path fill="#fc6d26" d="M32 61.477L43.784 25.2H60.3z"></path>
      <path
        fill="#fca326"
        d="M60.3 25.2l3.58 11.02a2.44 2.44 0 01-.886 2.728L32 61.477z"
      ></path>
      <path
        fill="#e24329"
        d="M60.3 25.2H43.784l7.098-21.844c.365-1.124 1.955-1.124 2.32 0z"
      ></path>
    </svg>
  );
}

export default Icon;
