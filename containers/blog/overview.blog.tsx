import {
  AvatarImage,
  IconComment,
  IconEye,
  IconHeartLight,
  IconUpload,
} from "@/assets";
import { getRandomColor } from "@/utils/random";
import Image from "next/image";
import React from "react";

const mockTag = ["FrontEnd", "BackEnd", "Devops", "Design"];

const OverviewBlog = () => {
  return (
    <div>
      <div className="max-w-screen-md mx-auto">
        <div className="flex items-center gap-x-2 mb-4">
          {mockTag.map((item, idx) => {
            const color = getRandomColor();

            return (
              <div
                key={idx}
                className={`text-red-800 text-xs bg-red-100 cursor-pointer font-medium hover:bg-red-800 w-fit px-3 transition-colors hover:text-white duration-300  rounded-full py-1`}
              >
                {item}
              </div>
            );
          })}
        </div>
        <h1 className="break-words text-black font-semibold text-3xl md:text-4xl md:!leading-[120%] lg:text-5xl dark:text-white max-w-4xl">
          VnTools v2 – Plugin đổi số thành chữ cho Excel
        </h1>
        <div className="w-full border-b border-black/10 my-5 dark:border-white/10" />
        <div className="flex max-md:flex-col gap-y-5 md:items-center justify-between">
          <div className="flex items-center gap-x-3">
            <Image
              src={AvatarImage}
              width={44}
              height={44}
              className="rounded-full"
              alt="Avatar"
            />
            <div className="space-y-1">
              <h3 className="text-sm font-medium">minhquan</h3>
              <p className="text-xs opacity-50">19/05/2023 · 3 minutes</p>
            </div>
          </div>
          <div className="flex items-center gap-x-2 h-full">
            <div className="dark:bg-dark-backgroundHover bg-backgroundHover justify-center min-w-[68px] dark:text-dark-subText text-subText py-1 px-3 rounded-full items-center flex gap-x-1">
              <IconEye /> <p className="text-sm">93</p>
            </div>
            <div className="hover:text-teal-500 hover:bg-teal-100 dark:hover:text-teal-500 dark:hover:bg-teal-100 min-w-[68px] bg-backgroundHover transition-colors cursor-pointer dark:bg-dark-backgroundHover dark:text-dark-subText text-subText py-1 px-3 rounded-full items-center justify-center flex gap-x-1">
              <IconComment /> <p className="text-sm">1</p>
            </div>
            <div className="h-6 border-l dark:border-white/20 border-black/20" />
            <div className="hover:text-red-500 hover:bg-red-100 dark:hover:text-red-500 dark:hover:bg-red-100 min-w-[68px] bg-backgroundHover transition-colors cursor-pointer dark:bg-dark-backgroundHover dark:text-dark-subText text-subText py-1 px-3 rounded-full items-center justify-center flex gap-x-1">
              <IconHeartLight /> <p className="text-sm">0</p>
            </div>
            <div className="hover:text-blue-500 hover:bg-blue-100 dark:hover:text-blue-500 dark:hover:bg-blue-100 bg-backgroundHover transition-colors cursor-pointer dark:bg-dark-backgroundHover dark:text-dark-subText text-subText py-1 px-1.5 rounded-full items-center justify-center flex gap-x-1">
              <IconUpload />
            </div>
          </div>
        </div>
      </div>
      <Image
        width="1280"
        height="720"
        src="https://images.unsplash.com/photo-1626968361222-291e74711449?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80"
        className="w-full m-0 rounded-xl max-h-[720px] overflow-hidden my-10"
        alt="Red Giant Trapcode Suite"
        sizes="(max-width: 1280px) 100vw, 1280px"
      />
    </div>
  );
};

export default OverviewBlog;
