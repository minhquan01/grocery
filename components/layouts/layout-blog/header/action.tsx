"use client";

import { IconMoon, IconSearch, IconSun } from "@/assets";
import ButtonCustom from "@/components/button.custom";
import { DropdownMenuCustom } from "@/components/dropdown.menu";
import { useAuth } from "@/components/providers/auth.provider";
import ToggleMode from "@/components/toggle.mode";
import { Button } from "@/components/ui/button";
import { useModal } from "@/hooks/use.modal.store";
import { Search, User } from "lucide-react";
import { useTheme } from "next-themes";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";

const data = [
  {
    title: "Profile",
    icon: <User className="h-4 w-4" />,
    url: "/profile",
  },
];

const Action = () => {
  const router = useRouter();
  const { onOpen } = useModal();
  const { user } = useAuth();

  return (
    <div className="flex items-center">
      <ToggleMode />
      <div
        onClick={() => onOpen("search")}
        className="hidden h-12 w-12 hover:bg-bgIconHover rounded-full dark:hover:bg-bgIconHoverDark md:flex items-center justify-center cursor-pointer select-none"
      >
        <IconSearch />
      </div>
      {user ? (
        <DropdownMenuCustom isLogout dataItem={data}>
          <p className="cursor-pointer ml-4">{user.username}</p>
        </DropdownMenuCustom>
      ) : (
        <ButtonCustom onClick={() => router.push("/sign-in")} className="ml-3">
          Đăng nhập
        </ButtonCustom>
      )}
    </div>
  );
};

export default Action;
