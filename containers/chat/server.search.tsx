"use client";

import CommandDialogCustom from "@/components/command.dialog.custom";
import FuncButton from "@/components/func.button";
import { CommandGroup, CommandItem } from "@/components/ui/command";
import useKeyboardShortcut from "@/hooks/use.keyboard.shortcut";

import { Search } from "lucide-react";
import { useParams, useRouter } from "next/navigation";
import React, { ReactNode, useEffect, useState } from "react";

interface ServerSearchProps {
  data: {
    label: string;
    type: "channel" | "member";
    data:
      | {
          icon: ReactNode;
          name: string;
          id: string;
        }[]
      | undefined;
  }[];
}

const ServerSearch = ({ data }: ServerSearchProps) => {
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const params = useParams();

  const toggleOpen = () => {
    setOpen((prev) => !prev);
  };

  useKeyboardShortcut("f", toggleOpen);

  const handleSelect = ({
    id,
    type,
  }: {
    id: string;
    type: "channel" | "member";
  }) => {
    setOpen(false);
    if (type === "member") {
      return router.push(`/servers/${params?.serverId}/conversations/${id}`);
    }

    if (type === "channel") {
      return router.push(`/servers/${params?.serverId}/channels/${id}`);
    }
  };

  return (
    <div>
      <button
        onClick={() => setOpen(true)}
        className="group px-2 py-2 rounded-md flex items-center gap-x-2 w-full dark:text-white/70 dark:hover:text-white hover:bg-zinc-700/10 dark:hover:bg-[#28303e] transition"
      >
        <Search className="w-4 h-4 -mt-0.5" />
        <p className="font-semibold text-sm">Tìm kiếm</p>
        <FuncButton characters="F" />
      </button>
      <CommandDialogCustom
        open={open}
        placeholder="Tìm kiếm tất cả kênh và thành viên"
        setOpen={setOpen}
      >
        {data.map(({ data, label, type }) => {
          if (!data?.length) return null;
          return (
            <CommandGroup key={label} heading={label}>
              {data.map(({ id, icon, name }) => (
                <CommandItem
                  key={id}
                  onSelect={() => handleSelect({ id, type })}
                >
                  {icon}
                  <span>{name}</span>
                </CommandItem>
              ))}
            </CommandGroup>
          );
        })}
      </CommandDialogCustom>
    </div>
  );
};

export default ServerSearch;
