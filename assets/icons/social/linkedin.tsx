import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="48"
      height="48"
      fill="none"
      viewBox="0 0 48 48"
    >
      <g clipPath="url(#clip0_17_32)">
        <path
          fill="#0A66C2"
          d="M44.457 0H3.543A3.543 3.543 0 000 3.543v40.914A3.543 3.543 0 003.543 48h40.914A3.543 3.543 0 0048 44.457V3.543A3.543 3.543 0 0044.457 0zm-30.15 40.89H7.09V17.967h7.217V40.89zm-3.614-26.1a4.143 4.143 0 114.167-4.14 4.083 4.083 0 01-4.167 4.14zm30.214 26.12h-7.214V28.387c0-3.694-1.57-4.834-3.596-4.834-2.14 0-4.24 1.614-4.24 4.927v12.43H18.64V17.983h6.94v3.177h.093c.697-1.41 3.137-3.82 6.86-3.82 4.027 0 8.377 2.39 8.377 9.39l-.003 14.18z"
        ></path>
      </g>
      <defs>
        <clipPath id="clip0_17_32">
          <path fill="#fff" d="M0 0H48V48H0z"></path>
        </clipPath>
      </defs>
    </svg>
  );
}

export default Icon;
