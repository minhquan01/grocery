"use client";

import HeaderBlog from "@/components/layouts/layout-blog/header";
import NavFooter from "@/components/layouts/layout-blog/header/navbar/nav.footer";
import SideBarBlog from "@/components/layouts/layout-blog/side.bar.blog";
import React, { ReactNode, useState } from "react";

interface BlogLayoutProps {
  children: ReactNode;
}

const BlogLayout = ({ children }: BlogLayoutProps) => {
  const [isOpenSideBar, setIsOpenSideBar] = useState(false);

  return (
    <div>
      <HeaderBlog />
      <NavFooter setOpenSide={setIsOpenSideBar} />
      <SideBarBlog
        isOpen={isOpenSideBar}
        onClose={() => setIsOpenSideBar(false)}
      />
      <main className="mt-4 max-md:pb-[60px] pb-20 max-w-main mx-auto">
        {children}
      </main>
    </div>
  );
};

export default BlogLayout;
