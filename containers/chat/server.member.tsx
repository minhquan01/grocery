"use client";

import { roleIconMap } from "@/constant/icon";
import { cn } from "@/lib/utils";
import { Member, Profile, RoleAccount, Server } from "@prisma/client";
import { useParams, useRouter } from "next/navigation";
import React from "react";

interface ServerMemberProps {
  member: Member & { profile: Profile };
  server: Server;
}

const ServerMember = ({ member, server }: ServerMemberProps) => {
  const params = useParams();
  const router = useRouter();

  const icon = roleIconMap[member.role];
  return (
    <button
      onClick={() =>
        router.push(`/servers/${params?.serverId}/conversations/${member.id}`)
      }
      className={cn(
        "group px-2 py-2 rounded-md flex items-center gap-x-2 w-full hover:bg-zinc-700/10 dark:hover:bg-zinc-700/50 transition mb-1",
        params?.memberId === member.id && "bg-zinc-700/20 dark:bg-zinc-700"
      )}
    >
      <img
        className="w-8 h-8 rounded-full object-cover"
        src={
          member.profile.avatar ??
          "https://i.pinimg.com/564x/ac/29/ad/ac29ad52d56454d2b1f77db59c366213.jpg"
        }
        alt=""
      />
      <p
        className={cn(
          "font-semibold text-sm text-zinc-500 group-hover:text-zinc-600 dark:text-zinc-400 dark:group-hover:text-zinc-300 transition",
          params?.channelId === member.id && "text-primary"
        )}
      >
        {member.profile.name}
      </p>
      {icon}
    </button>
  );
};

export default ServerMember;
