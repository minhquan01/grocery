import React from "react";
import UploadImageContainer from "../upload.image.container";
import { Camera, Plus } from "lucide-react";

interface UploadAvatarProps {
  onChange: (url?: string) => void;
  value: string;
}

const UploadAvatar = ({ onChange, value }: UploadAvatarProps) => {
  return (
    <UploadImageContainer onChange={onChange} value={value}>
      <div className="h-24 w-24 relative rounded-full border border-dashed flex gap-y-2 flex-col justify-center items-center">
        <Camera />
        <p className="uppercase text-xs font-bold">Tải lên</p>
        <div className="bg-primary absolute top-0 right-2 rounded-full p-0.5">
          <Plus className="w-4 h-4" />
        </div>
      </div>
    </UploadImageContainer>
  );
};

export default UploadAvatar;
