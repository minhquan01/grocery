import { useModal } from "@/hooks/use.modal.store";
import { ServerWithMembersWithProfiles } from "@/type";
import { InitModal } from "./init.modal";
import { ScrollArea } from "../ui/scroll-area";
import {
  Check,
  Gavel,
  Loader2,
  MoreVertical,
  Shield,
  ShieldAlert,
  ShieldCheck,
  ShieldQuestion,
  UserPlus2,
} from "lucide-react";
import { useState } from "react";
import { DropdownMenuCustom } from "../dropdown.menu";
import { RoleAccount } from "@prisma/client";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuPortal,
  DropdownMenuSeparator,
  DropdownMenuSub,
  DropdownMenuSubContent,
  DropdownMenuSubTrigger,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import qs from "query-string";
import { editRoleMember, kickMember } from "@/services/chat";
import { useRouter } from "next/navigation";

const roleIconMap = {
  GUEST: null,
  EDITOR: <ShieldCheck className="h-4 w-4 text-indigo-500" />,
  ADMIN: <ShieldAlert className="h-4 w-4 text-rose-500" />,
};

const MemberModal = () => {
  const { isOpen, onClose, type, data, onOpen } = useModal();
  const [loadingId, setLoadingId] = useState("");

  const isModalOpen = isOpen && type === "manageMember";
  const { server } = data as { server: ServerWithMembersWithProfiles };

  const router = useRouter();
  const onKick = async (memberId: string) => {
    try {
      setLoadingId(memberId);
      const query = {
        serverId: server.id,
      };
      const response = await kickMember(memberId, query);
      const serverUpdate = response.data.server;
      if (serverUpdate) {
        router.refresh();
        onOpen("manageMember", { server: serverUpdate });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingId("");
    }
  };

  const onRoleChange = async (memberId: string, role: RoleAccount) => {
    try {
      setLoadingId(memberId);

      const query = {
        serverId: server.id,
      };

      const response = await editRoleMember(memberId, role, query);
      const serverUpdate = response.data.server;
      if (serverUpdate) {
        router.refresh();
        onOpen("manageMember", { server: serverUpdate });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingId("");
    }
  };
  return (
    <InitModal
      action={false}
      title="Quản lý thành viên"
      isModalOpen={isModalOpen}
      description={`${server?.members?.length} Member`}
      onClose={onClose}
    >
      <ScrollArea className="mt-8 max-h-[420px] pr-6">
        {server?.members?.map((member) => (
          <div key={member.id} className="flex items-center gap-2 mb-6">
            <img
              className="w-10 h-10 rounded-full object-cover"
              src={
                member.profile.avatar ??
                "https://i.pinimg.com/564x/ac/29/ad/ac29ad52d56454d2b1f77db59c366213.jpg"
              }
              alt=""
            />
            <div className="flex flex-col gap-y-1">
              <div className="text-sm font-semibold flex items-center gap-x-2">
                {member.profile.name}
                {roleIconMap[member.role]}
              </div>
            </div>
            {server?.profileId !== member.profileId &&
              loadingId !== member.id && (
                <div className="ml-auto relative z-[99] outline-none">
                  <DropdownMenu>
                    <DropdownMenuTrigger>
                      <MoreVertical className="h-4 w-4 text-zinc-500" />
                    </DropdownMenuTrigger>
                    <DropdownMenuContent side="left">
                      <DropdownMenuSub>
                        <DropdownMenuSubTrigger className="flex items-center">
                          <ShieldQuestion className="w-4 h-4 mr-2" />
                          <span>Role</span>
                        </DropdownMenuSubTrigger>
                        <DropdownMenuPortal>
                          <DropdownMenuSubContent className="z-[999] ml-2 -mt-0.5">
                            <DropdownMenuItem
                              onClick={() => onRoleChange(member.id, "GUEST")}
                            >
                              <Shield className="h-4 w-4 mr-2" />
                              Guest
                              {member.role === "GUEST" && (
                                <Check className="h-4 w-4 ml-auto" />
                              )}
                            </DropdownMenuItem>
                            <DropdownMenuItem
                              onClick={() => onRoleChange(member.id, "EDITOR")}
                            >
                              <ShieldCheck className="h-4 w-4 mr-2" />
                              Editor
                              {member.role === "EDITOR" && (
                                <Check className="h-4 w-4 ml-auto" />
                              )}
                            </DropdownMenuItem>
                          </DropdownMenuSubContent>
                        </DropdownMenuPortal>
                      </DropdownMenuSub>
                      <DropdownMenuSeparator />
                      <DropdownMenuItem onClick={() => onKick(member.id)}>
                        <Gavel className="h-4 w-4 mr-2" />
                        Kick
                      </DropdownMenuItem>
                    </DropdownMenuContent>
                  </DropdownMenu>
                </div>
              )}
            {loadingId === member.id && (
              <Loader2 className="animate-spin text-zinc-500 ml-auto w-4 h-4" />
            )}
          </div>
        ))}
      </ScrollArea>
    </InitModal>
  );
};

export default MemberModal;
