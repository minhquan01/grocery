import axios from "axios";

export const axiosCustom = axios.create({
  baseURL: "/api",
});

// Thêm một interceptor yêu cầu để đính kèm access token vào các yêu cầu đi ra
axiosCustom.interceptors.request.use(
  (config) => {
    const accessToken = localStorage.getItem("accessToken");

    if (accessToken) {
      config.headers["Authorization"] = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
// Thêm một interceptor phản hồi để xử lý việc hết hạn token và làm mới
axiosCustom.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    if (error.response && error.response.status === 401) {
      // Token hết hạn, thử làm mới nó

      try {
        const refreshToken = localStorage.getItem("refreshToken");
        if (!refreshToken) return;
        const config = {
          headers: {
            Authorization: `Bearer ${refreshToken}`,
          },
        };
      } catch (error) {
        return;
      }
    }
    return Promise.reject(error.response.data);
  }
);
