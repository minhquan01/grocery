import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { cn } from "@/lib/utils";
import { X } from "lucide-react";
import { ReactNode } from "react";

interface InitModalProps {
  title?: string;
  description?: ReactNode | string;
  children: ReactNode;
  textSubmit?: String;
  textCancel?: string;
  isModalOpen: boolean;
  onClose: () => void;
  action?: boolean;
  noBg?: boolean;
}

export function InitModal({
  title,
  description,
  children,
  textSubmit,
  textCancel,
  isModalOpen,
  onClose,
  action = true,
  noBg = false,
}: InitModalProps) {
  return (
    <Dialog open={isModalOpen} onOpenChange={onClose}>
      <DialogContent
        className={cn(
          "mx-auto",
          noBg ? "bg-transparent border-none" : "bg-dark-backgroundHover"
        )}
      >
        {(title || description) && (
          <DialogHeader className="flex items-center justify-center">
            <DialogTitle className="text-xl capitalize font-black">
              {title}
            </DialogTitle>
            <DialogDescription className="text-center">
              {description}
            </DialogDescription>
            {!noBg && (
              <div
                onClick={onClose}
                className="absolute top-1 right-3 cursor-pointer hover:text-red-300"
              >
                <X className="h-4 w-4" />
                <span className="sr-only">Close</span>
              </div>
            )}
          </DialogHeader>
        )}
        {children}
        {action && (
          <DialogFooter>
            <Button type="submit" variant="outline">
              {textCancel ?? "Huỷ"}
            </Button>
            <Button type="submit">{textSubmit ?? "Lưu"}</Button>
          </DialogFooter>
        )}
      </DialogContent>
    </Dialog>
  );
}
