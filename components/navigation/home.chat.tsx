"use client";

import { cn } from "@/lib/utils";
import { usePathname, useRouter } from "next/navigation";
import React from "react";
import ActionTooltip from "../action.tooltip";

const HomeChat = () => {
  const pathname = usePathname();
  const router = useRouter();
  return (
    <ActionTooltip className="-mt-2" label="Tin nhắn trực tiếp" align="start">
      <button
        onClick={() => router.push("/chat")}
        className="group relative flex items-center"
      >
        <div
          className={cn(
            "absolute top-0 bottom-0 my-auto left-0 bg-white rounded-r-full transition-all w-1",
            pathname === "/chat" ? "h-8" : "h-3 group-hover:h-8"
          )}
        ></div>
        <div
          className={`${
            pathname === "/chat"
              ? "bg-primary rounded-2xl"
              : "group-hover:rounded-2xl rounded-3xl bg-black dark:bg-[#2c3647] group-hover:bg-emerald-500"
          } flex mx-3 h-12 w-12 transition-all overflow-hidden items-center justify-center `}
        >
          <p className="text-xl mt-1 font-bold">P</p>
        </div>
      </button>
    </ActionTooltip>
  );
};

export default HomeChat;
