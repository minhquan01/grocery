"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { useModal } from "@/hooks/use.modal.store";
import { zodResolver } from "@hookform/resolvers/zod";
import { useRouter } from "next/navigation";

import ButtonCustom from "../button.custom";
import { Input } from "../ui/input";
import UploadImage from "../upload.image";
import { InitModal } from "./init.modal";
import RenderInputFrom from "../form/render.input";
import { UploadDropzone } from "@/utils/uploadthing";
import UploadAvatar from "../upload/upload.avatar";
import { createServerChat, editServerChat } from "@/services/chat";
import { useEffect } from "react";
const formSchema = z.object({
  name: z.string().min(1, {
    message: "Server name is required.",
  }),
  avatar: z.string().min(1, {
    message: "Server image is required.",
  }),
});
const EditServerModal = () => {
  const { isOpen, onClose, type, data } = useModal();
  const router = useRouter();
  const { server } = data;

  const isModalOpen = isOpen && type === "editServer";

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      avatar: "",
    },
  });

  useEffect(() => {
    if (server) {
      form.setValue("avatar", server.avatar);
      form.setValue("name", server.name);
    }
  }, [server, isModalOpen]);

  const isLoading = form.formState.isSubmitting;

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      if (!server) return;
      await editServerChat(values, server);
      form.reset();
      router.refresh();
      onClose();
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    form.reset();
    onClose();
  };

  return (
    <InitModal
      action={false}
      title="Phòng của bạn"
      description="Chỉnh sửa máy chủ mới của bạn một cá tính riêng bằng tên và biểu tượng mới."
      isModalOpen={isModalOpen}
      onClose={handleClose}
    >
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <div className="space-y-8 ">
            <p className="flex items-center justify-center text-center">
              <FormField
                control={form.control}
                name="avatar"
                render={({ field }) => {
                  return (
                    <FormItem>
                      <FormControl>
                        {/* <UploadImage
                              endpoint="serverImage"
                              
                            /> */}
                        <UploadAvatar
                          value={field.value}
                          onChange={field.onChange}
                        />
                      </FormControl>
                    </FormItem>
                  );
                }}
              />
            </p>
            <RenderInputFrom form={form} label="Tên phòng" name="name" />
          </div>
          <div className="flex items-center justify-between">
            <ButtonCustom
              className="w-full"
              isLoading={isLoading}
              type="submit"
            >
              Lưu
            </ButtonCustom>
          </div>
        </form>
      </Form>
    </InitModal>
  );
};

export default EditServerModal;
