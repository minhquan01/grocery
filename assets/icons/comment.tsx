import React from "react";

function Icon() {
  return (
    <svg width="24" height="24" fill="none" viewBox="0 0 24 24">
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M4.75 6.75a2 2 0 012-2h10.5a2 2 0 012 2v7.5a2 2 0 01-2 2h-2.625l-2.625 3-2.625-3H6.75a2 2 0 01-2-2v-7.5z"
      ></path>
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M9.5 11a.5.5 0 11-1 0 .5.5 0 011 0zM12.5 11a.5.5 0 11-1 0 .5.5 0 011 0zM15.5 11a.5.5 0 11-1 0 .5.5 0 011 0z"
      ></path>
    </svg>
  );
}

export default Icon;
