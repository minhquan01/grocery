import { NextResponse } from "next/server";

export async function POST(req: Request) {
  try {
    const body = await req.json();
  } catch (error) {
    return NextResponse.json({
      error,
      status: 500,
    });
  }
}
