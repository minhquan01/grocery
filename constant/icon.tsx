import { ChannelType, RoleAccount } from "@prisma/client";
import { Hash, Mic, ShieldAlert, ShieldCheck, Video } from "lucide-react";

export const iconMap = {
  [ChannelType.TEXT]: <Hash className="mr-2 h-4 w-4" />,
  [ChannelType.AUDIO]: <Mic className="mr-2 h-4 w-4" />,
  [ChannelType.VIDEO]: <Video className="mr-2 h-4 w-4" />,
};

export const roleIconMap = {
  [RoleAccount.GUEST]: null,
  [RoleAccount.EDITOR]: <ShieldCheck className="h-4 w-4 mr-2 text-green-500" />,
  [RoleAccount.ADMIN]: <ShieldAlert className="h-4 w-4 mr-2 text-rose-500" />,
};
