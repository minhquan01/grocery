import { useModal } from "@/hooks/use.modal.store";
import { RefreshCw } from "lucide-react";
import ButtonCustom from "../button.custom";
import InputCustom from "../input.custom";
import { InitModal } from "./init.modal";
import { useOrigin } from "@/hooks/use.origin";
import { useState } from "react";
import { useCopy } from "@/hooks/use.copy";
import { axiosCustom } from "@/services/axios.cutom";

const InviteMemberModal = () => {
  const { copied, onCopy } = useCopy();
  const [isLoading, setIsLoading] = useState(false);

  const { isOpen, onClose, type, data, onOpen } = useModal();
  const origin = useOrigin();

  const isModalOpen = isOpen && type === "inviteMember";
  const { server } = data;

  const inviteUrl = `${origin}/invite/${server?.inviteCode}`;

  const copyLink = () => {
    onCopy(inviteUrl);
  };

  const onNew = async () => {
    try {
      setIsLoading(true);
      const response = await axiosCustom.patch(
        `/server/${server?.id}/invite-code`
      );
      onOpen("inviteMember", { server: response.data });
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <InitModal
      action={false}
      title="Mời bạn bè"
      isModalOpen={isModalOpen}
      onClose={onClose}
    >
      <div className="mt-8">
        <InputCustom
          copied={copied}
          onCopyCustom={copyLink}
          label="Link mời thành viên"
          variant="fill"
          value={inviteUrl}
          readOnly
          loading={isLoading}
          disabled
          copy
        />
      </div>
      <ButtonCustom
        onClick={onNew}
        variant="link"
        disabled={isLoading}
        size="sm"
        className="text-xs flex items-center gap-x-2 text-zinc-500 mt-4 w-fit"
      >
        <p>Tạo đường link mới</p>
        <RefreshCw className="h-4 w-4" />
      </ButtonCustom>
    </InitModal>
  );
};

export default InviteMemberModal;
