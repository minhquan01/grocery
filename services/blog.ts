import axios from "axios";

export const createBlog = async () => {
  return await axios.post("/api/blog/create");
};
