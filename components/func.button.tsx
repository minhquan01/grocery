import React from "react";

interface FuncButtonProps {
  characters: string;
}

const FuncButton = ({ characters }: FuncButtonProps) => {
  return (
    <kbd className="pointer-events-none inline-flex h-5 select-none items-center bg-[#1F2937] border-white/5 gap-[3px] rounded border px-1.5 font-mono text-[10px] font-medium ml-auto">
      <span className="text-sm">⌘</span>
      <span className="font-medium text-xs">{characters}</span>
    </kbd>
  );
};

export default FuncButton;
