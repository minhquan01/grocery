import CreateBlog from "@/containers/blog/create.blog";

const NewPost = () => {
  return <div>{typeof window !== "undefined" && <CreateBlog />}</div>;
};

export default NewPost;
