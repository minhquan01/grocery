export const dynamic = "force-dynamic";

import { secretKey } from "@/constant";
import jwt from "jsonwebtoken";
import { NextResponse } from "next/server";

export async function GET(req: Request) {
  try {
    const token = req.headers.get("Authorization");
    if (!token) {
      return NextResponse.json({ message: "Unauthorized", status: 401 });
    }
    const tokenWithoutBearer = token.replace("Bearer ", "");
    const decoded = jwt.verify(tokenWithoutBearer, secretKey);

    return NextResponse.json({ status: 200, user: decoded });
  } catch (error) {
    console.log({ error });
    return NextResponse.json({
      error,
      status: 500,
    });
  }
}
