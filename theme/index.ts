export const lightThemeToast = {
  style: {
    borderRadius: "99px",
    background: "#fff",
    color: "#000",
  },
};

export const darkThemeToast = {
  style: {
    borderRadius: "99px",
    background: "#333",
    color: "#fff",
  },
};
