import Content from "@/components/content";
import BlogDetailContent from "@/containers/blog/blog.detail.content";
import React from "react";

const BlogDetail = () => {
  return (
    <Content>
      <BlogDetailContent />
    </Content>
  );
};

export default BlogDetail;
