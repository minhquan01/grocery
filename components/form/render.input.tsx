"use client";

import React, { useState } from "react";
import {
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "../ui/form";
import { Input } from "../ui/input";
import { VscEye, VscEyeClosed } from "react-icons/vsc";

interface RenderInputFromProps {
  form: any;
  name: string;
  label: string;
  isLoading?: boolean;
  type?: React.HTMLInputTypeAttribute;
}

const RenderInputFrom = ({
  form,
  name,
  isLoading,
  label,
  type = "text",
}: RenderInputFromProps) => {
  const [typeCurrent, setTypeCurrent] = useState(type);

  return (
    <FormField
      control={form.control}
      name={name}
      render={({ field }) => (
        <FormItem>
          <FormLabel className="text-sm md:text-base font-medium text-zinc-500 dark:text-white/80">
            {label}
          </FormLabel>
          <FormControl>
            <div className="relative w-full">
              <Input
                type={typeCurrent}
                disabled={isLoading}
                placeholder="Nhập..."
                {...field}
              />
              {type === "password" && (
                <div
                  onClick={() => {
                    if (type === typeCurrent) {
                      setTypeCurrent("text");
                    } else {
                      setTypeCurrent(type);
                    }
                  }}
                  className="absolute cursor-pointer p-1 right-5 top-0 bottom-0 my-auto h-fit"
                >
                  {type === typeCurrent ? <VscEye /> : <VscEyeClosed />}
                </div>
              )}
            </div>
          </FormControl>
          <FormMessage />
        </FormItem>
      )}
    />
  );
};

export default RenderInputFrom;
