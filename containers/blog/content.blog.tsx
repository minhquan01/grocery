import Link from "next/link";
import React from "react";

const rencentPost = [
  {
    url: "/12321",
    title: "ProfiCAD 2023 – Thiết kế sơ đồ mạch điện",
  },
  {
    url: "/12321",
    title: "ProfiCAD 2023 – Thiết kế sơ đồ mạch điện",
  },
  {
    url: "/12321",
    title: "ProfiCAD 2023 – Thiết kế sơ đồ mạch điện",
  },
  {
    url: "/12321",
    title: "ProfiCAD 2023 – Thiết kế sơ đồ mạch điện",
  },
  {
    url: "/12321",
    title: "ProfiCAD 2023 – Thiết kế sơ đồ mạch điện",
  },
];

const ContentBlog = () => {
  return (
    <div className="grid grid-col-1 md:grid-cols-9 gap-4">
      <div className="md:col-span-6 bg-red-500">asd</div>
      <div className="md:col-span-3">
        <div className="dark:bg-dark-box bg-box rounded-xl py-2 ">
          <h2 className="text-lg font-medium py-2 px-3">Bài viết gần đây</h2>
          <div className="flex flex-col ">
            {rencentPost.map((item, idx) => (
              <Link
                key={idx}
                href={item.url}
                className="py-3 border-t px-3 dark:border-white/10 dark:hover:bg-white/5 dark:text-[#D1D5DB] dark:hover:text-primary hover:text-primary text-black/90"
              >
                {item.title}
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentBlog;
