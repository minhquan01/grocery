"use client";

import React from "react";
import SearchModal from "../modal/search.modal";
import CreateServerModal from "../modal/create.server.modal";
import InviteMemberModal from "../modal/invite.member.modal";
import EditServerModal from "../modal/edit.server.modal";
import MemberModal from "../modal/member.modal";
import CreateChannelModal from "../modal/create.channel.modal";
import LeaveServerModal from "../modal/leave.server.modal";
import DeleteServerModal from "../modal/delete.server.modal";
import DeleteChannelModal from "../modal/delete.channel.modal";
import EditChannelModal from "../modal/edit.channel.modal";
import MessageFileModal from "../modal/message.file.modal";

const ModalProvider = () => {
  return (
    <>
      <SearchModal />
      <CreateServerModal />
      <InviteMemberModal />
      <EditServerModal />
      <MemberModal />
      <CreateChannelModal />
      <LeaveServerModal />
      <DeleteServerModal />
      <DeleteChannelModal />
      <EditChannelModal />
      <MessageFileModal />
    </>
  );
};

export default ModalProvider;
