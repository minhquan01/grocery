import React from "react";

function Icon() {
  return (
    <svg width="24" height="24" fill="none" viewBox="0 0 24 24">
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M4.75 14.75v1.5a3 3 0 003 3h8.5a3 3 0 003-3v-1.5M12 14.25V5M8.75 8.25L12 4.75l3.25 3.5"
      ></path>
    </svg>
  );
}

export default Icon;
