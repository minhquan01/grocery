import SignInContent from "@/containers/auth/sign.in";
import React from "react";

const SignIn = () => {
  return (
    <div>
      <SignInContent />
    </div>
  );
};

export default SignIn;
