import React, { ReactNode } from "react";

interface ContentProps {
  children: ReactNode;
}

const Content = ({ children }: ContentProps) => {
  return <div className="max-w-mobile md:max-w-main mx-auto">{children}</div>;
};

export default Content;
