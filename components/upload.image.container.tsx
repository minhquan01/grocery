import { useUploadThing } from "@/utils/uploadthing";
import { FileWithPath } from "@uploadthing/react";
import { useDropzone } from "@uploadthing/react/hooks";
import React, { ReactNode, useCallback, useState } from "react";
import { generateClientDropzoneAccept } from "uploadthing/client";
import ButtonCustom from "./button.custom";
import Image from "next/image";
import { FileIcon, X } from "lucide-react";

interface UploadImageContainerProps {
  children: ReactNode;
  onChange: (url?: string) => void;
  value: string;
}

const UploadImageContainer = ({
  children,
  onChange,
  value,
}: UploadImageContainerProps) => {
  const [files, setFiles] = useState<File[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const onDrop = useCallback((acceptedFiles: FileWithPath[]) => {
    setFiles(acceptedFiles);
  }, []);

  const { startUpload, permittedFileInfo } = useUploadThing("serverImage", {
    onClientUploadComplete: (res) => {
      onChange(res?.[0].fileUrl);
      setIsLoading(false);
    },
    onUploadError: (error) => {
      console.log(error);
    },
  });
  const fileTypes = permittedFileInfo?.config
    ? Object.keys(permittedFileInfo?.config)
    : [];
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: fileTypes ? generateClientDropzoneAccept(fileTypes) : undefined,
  });
  const fileType = value?.split(".").pop();

  console.log({ fileType });

  if (value && fileType !== "pdf") {
    return (
      <div className="relative h-20 w-20">
        <Image fill src={value} alt="Avatar" className="rounded-full" />
        {value && (
          <button
            onClick={() => {
              onChange("");
              setFiles([]);
            }}
            className="bg-rose-500 text-white p-1 rounded-full absolute top-0 right-0 shadow-sm"
            type="button"
          >
            <X className="w-3 h-3" />
          </button>
        )}
      </div>
    );
  }

  if (value && fileType === "pdf") {
    return (
      <div className="relative file items-center p-2 mt-2 rounded-md">
        <FileIcon className="h-10 w-10 fill-indigo-200 stroke-indigo-400" />
        <a
          href={value}
          target="_blank"
          rel="noopener noreferrer"
          className="ml-2 text-sm text-indigo-500 dark:text-indigo-400 hover:underline"
        >
          {value}
        </a>
        {value && (
          <button
            onClick={() => {
              onChange("");
            }}
            className="bg-rose-500 text-white p-1 rounded-full absolute top-0 right-0 shadow-sm"
            type="button"
          >
            <X className="w-3 h-3" />
          </button>
        )}
      </div>
    );
  }
  return (
    <div className="flex flex-col items-center justify-center gap-y-2">
      <div {...getRootProps()} className="cursor-pointer">
        <input {...getInputProps()} />
        {children}
      </div>
      <div>
        {files.length > 0 && (
          <ButtonCustom
            isLoading={isLoading}
            size={"sm"}
            onClick={(e) => {
              e.preventDefault();
              startUpload(files);
              setIsLoading(true);
            }}
          >
            Tải lên {files.length} files
          </ButtonCustom>
        )}
      </div>
    </div>
  );
};

export default UploadImageContainer;
