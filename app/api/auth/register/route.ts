import { db } from "@/lib/prisma";
import bcrypt from "bcrypt";
import { NextResponse } from "next/server";

export async function POST(req: Request) {
  const salt = await bcrypt.genSalt(10);

  try {
    const body = await req.json();
    const { username, name, password } = body;
    const currentAcc = await db.account.findUnique({
      where: {
        username: username,
      },
    });
    if (currentAcc) {
      return NextResponse.json({
        status: 409,
        statusText: "Tên đăng nhập đã được sử dụng",
      });
    }

    const hashed = await bcrypt.hash(password, salt);
    const account = await db.account.create({
      data: {
        password: hashed,
        username: username,
      },
    });
    await db.profile.create({
      data: {
        account: { connect: { username: account.username } },
        name: name,
      },
    });
    return NextResponse.json({
      account: account,
      message: "Tạo tài khoản thành công",
      status: 201,
    });
  } catch (error) {
    return NextResponse.json({
      error,
      status: 500,
    });
  }
}
