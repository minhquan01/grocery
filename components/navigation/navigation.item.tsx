"use client";

import React from "react";
import ActionTooltip from "../action.tooltip";
import { cn } from "@/lib/utils";
import { useParams, useRouter } from "next/navigation";
import Image from "next/image";

interface NavigationItemProps {
  id: string;
  avatar: string;
  name: string;
}

const NavigationItem = ({ id, avatar, name }: NavigationItemProps) => {
  const params = useParams();
  const router = useRouter();

  const handleRedirectServer = () => {
    router.push(`/servers/${id}`);
  };

  return (
    <ActionTooltip label={name}>
      <button
        onClick={handleRedirectServer}
        className="group relative flex items-center w-full h-fit"
      >
        <div
          className={cn(
            "absolute top-0 bottom-0 my-auto left-0 bg-white rounded-r-full transition-all w-1",
            params?.serverId === id ? "h-8" : "h-3 group-hover:h-8"
          )}
        ></div>
        <div
          className={cn(
            "relative group flex mx-3 h-12 w-12 rounded-3xl group-hover:rounded-2xl transition-all overflow-hidden",
            params?.serverId === id &&
              "bg-primary-green/10 text-white rounded-2xl"
          )}
        >
          <Image fill src={avatar} alt="Channel" />
        </div>
      </button>
    </ActionTooltip>
  );
};

export default NavigationItem;
