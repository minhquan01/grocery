import MobileToggle from "@/components/mobile.toggle";
import SocketIndicator from "@/components/socket.indicator";
import { Hash, Menu } from "lucide-react";
import React from "react";

interface ChatHeaderProps {
  serverId: string;
  name: string;
  type: "channel" | "conversation";
  imageUrl?: string;
}

const ChatHeader = ({ imageUrl, name, serverId, type }: ChatHeaderProps) => {
  console.log({ imageUrl });
  return (
    <div className="px-4 py-3 border-b w-full border-[#1B222E] flex items-center cursor-pointer shadow-md">
      <MobileToggle serverId={serverId} />
      {type === "channel" ? (
        <Hash className="w-5 h-5 text-zinc-500 dark:text-zinc-400 mr-2" />
      ) : (
        <img
          className="w-8 h-8 rounded-full object-cover mr-2"
          src={
            imageUrl ??
            "https://i.pinimg.com/564x/ac/29/ad/ac29ad52d56454d2b1f77db59c366213.jpg"
          }
          alt=""
        />
      )}

      <p className="font-semibold text-black dark:text-white">{name}</p>
      <div className="ml-auto flex items-center">
        <SocketIndicator />
      </div>
    </div>
  );
};

export default ChatHeader;
