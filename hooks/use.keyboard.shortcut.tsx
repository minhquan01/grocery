"use client";

import { useEffect } from "react";

const useKeyboardShortcut = (key: string, callback: () => void) => {
  const handler = (e: KeyboardEvent) => {
    if (e.key === key.toLowerCase()) {
      e.preventDefault();
      callback();
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      document.addEventListener("keydown", handler);

      return () => {
        document.removeEventListener("keydown", handler);
      };
    }
  }, [key, callback]);
};

export default useKeyboardShortcut;
