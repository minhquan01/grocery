import React, { ReactNode } from "react";

interface BoxGlassProps {
  children: ReactNode;
}

const BoxGlass = ({ children }: BoxGlassProps) => {
  return (
    <div className="p-4 md:p-8 xl:py-14 md:px-10 bg-white/5 backdrop-filter backdrop-blur-xl shadow-lg rounded-xl md:rounded-3xl ">
      {children}
    </div>
  );
};

export default BoxGlass;
