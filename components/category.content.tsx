import React from "react";

interface CategoryContentProps {
  name: string;
}

const CategoryContent = ({ name }: CategoryContentProps) => {
  return (
    <div
      className={`text-red-800 text-xs bg-red-100 cursor-pointer font-medium hover:bg-red-800 capitalize w-fit px-3 transition-colors hover:text-white duration-300  rounded-full py-1`}
    >
      {name}
    </div>
  );
};

export default CategoryContent;
