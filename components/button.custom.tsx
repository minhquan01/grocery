import React, { ReactNode } from "react";
import { Button, ButtonProps } from "./ui/button";
import { Loader2 } from "lucide-react";

interface ButtonCustomProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    ButtonProps {
  isLoading?: boolean;
  className?: string;
  children: ReactNode;
  type?: "button" | "submit" | "reset" | undefined;
}

const ButtonCustom = ({
  isLoading,
  className,
  children,
  type = "button",
  ...props
}: ButtonCustomProps) => {
  return (
    <Button disabled={isLoading} type={type} className={className} {...props}>
      {isLoading && <Loader2 className="mr-2 h-4 w-4 animate-spin" />}
      {children}
    </Button>
  );
};

export default ButtonCustom;
