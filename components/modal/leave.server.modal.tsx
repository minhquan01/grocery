import { useCopy } from "@/hooks/use.copy";
import { useModal } from "@/hooks/use.modal.store";
import { useState } from "react";
import ButtonCustom from "../button.custom";
import { InitModal } from "./init.modal";
import { leaveServer } from "@/services/chat";
import { useRouter } from "next/navigation";

const LeaveServerModal = () => {
  const [isLoading, setIsLoading] = useState(false);

  const { isOpen, onClose, type, data } = useModal();

  const router = useRouter();
  const isModalOpen = isOpen && type === "leaveServer";
  const { server } = data;

  const handleLeave = async () => {
    try {
      if (!server?.id) return;
      setIsLoading(true);
      const res = await leaveServer(server?.id);
      if (res.status === 200) {
        onClose();
        router.refresh();
        router.push("/chat");
      }
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <InitModal
      action={false}
      title="Rời khỏi máy chủ"
      isModalOpen={isModalOpen}
      description={
        <div className="flex items-center gap-x-1 text-zinc-500">
          Bạn có chắc muốn rời khỏi máy chủ{" "}
          <span className="font-bold text-primary">{server?.name}</span>?
        </div>
      }
      onClose={onClose}
    >
      <div className="flex items-center justify-around mt-8">
        <ButtonCustom
          className="bg-red-500 w-full"
          onClick={handleLeave}
          isLoading={isLoading}
          type="submit"
        >
          Oke fen!!
        </ButtonCustom>
      </div>
    </InitModal>
  );
};

export default LeaveServerModal;
