import { DropdownMenuCustom } from "@/components/dropdown.menu";
import { ServerWithMembersWithProfiles } from "@/type";
import { RoleAccount } from "@prisma/client";
import {
  Delete,
  DeleteIcon,
  LogOut,
  PlusCircle,
  Settings,
  Trash,
  User,
  UserPlus2,
  Users,
} from "lucide-react";
import React from "react";
import { MdKeyboardArrowDown } from "react-icons/md";

interface ServerHeaderProps {
  server: ServerWithMembersWithProfiles;
  role?: RoleAccount;
}

const ServerHeader = ({ server, role }: ServerHeaderProps) => {
  const data = [
    {
      title: "Mời bạn bè",
      icon: <UserPlus2 className="h-4 w-4" />,
      modal: "inviteMember",
      className: "text-primary",
      role: [RoleAccount.ADMIN, RoleAccount.EDITOR],
      dataModal: { server },
    },
    {
      title: "Cài Đặt Máy Chủ",
      icon: <Settings className="h-4 w-4" />,
      modal: "editServer",
      role: [RoleAccount.ADMIN],
      dataModal: { server },
    },
    {
      title: "Quản lý thành viên",
      icon: <Users className="h-4 w-4" />,
      modal: "manageMember",
      role: [RoleAccount.ADMIN],
      dataModal: { server },
    },
    {
      title: "Tạo kênh",
      icon: <PlusCircle className="h-4 w-4" />,
      modal: "createChanel",
      role: [RoleAccount.ADMIN, RoleAccount.EDITOR],
    },
    {
      title: "Xoá máy chủ",
      icon: <Trash className="h-4 w-4" />,
      modal: "deleteServer",
      role: [RoleAccount.ADMIN],
      className: "text-rose-500",
      dataModal: { server },
    },
    {
      title: "Rời máy chủ",
      icon: <LogOut className="h-4 w-4" />,
      modal: "leaveServer",
      role: [RoleAccount.EDITOR, RoleAccount.GUEST],
      dataModal: { server },
      className: "text-rose-500",
    },
  ];

  return (
    <DropdownMenuCustom
      capitalize
      variant="between"
      dataItem={data}
      role={role}
    >
      <div className="px-4 py-3 border-b w-full border-black/10 flex items-center justify-between hover:bg-[#1e2531] cursor-pointer shadow-md">
        {server.name}
        <div>
          <MdKeyboardArrowDown />
        </div>
      </div>
    </DropdownMenuCustom>
  );
};

export default ServerHeader;
