import {
  AvatarImage,
  IconComment,
  IconEye,
  IconHeartLight,
  IconUpload,
} from "@/assets";
import { getRandomColor } from "@/utils/random";
import Image from "next/image";
import React from "react";
import { AiOutlineEye } from "react-icons/ai";
import OverviewBlog from "./overview.blog";
import ContentBlog from "./content.blog";

const BlogDetailContent = () => {
  return (
    <div className="pt-8 lg:pt-14">
      <OverviewBlog />
      <ContentBlog />
    </div>
  );
};

export default BlogDetailContent;
