"use client";

import ActionTooltip from "@/components/action.tooltip";
import { useModal } from "@/hooks/use.modal.store";
import { ServerWithMembersWithProfiles } from "@/type";
import { ChannelType, RoleAccount } from "@prisma/client";
import { Plus, Settings } from "lucide-react";
import React from "react";

interface ServerSectionProps {
  label: string;
  role?: RoleAccount;
  sectionType: "channels" | "members";
  channelType?: ChannelType;
  server?: ServerWithMembersWithProfiles;
}

const ServerSection = ({
  label,
  sectionType,
  channelType,
  role,
  server,
}: ServerSectionProps) => {
  const { onOpen } = useModal();

  return (
    <div className="flex items-center justify-between py-2">
      <p className="text-sm capitalize font-bold dark:text-zinc-400">{label}</p>
      {role !== RoleAccount.GUEST && sectionType === "channels" && (
        <ActionTooltip label="Tạo kênh" side="top">
          <button
            onClick={() => onOpen("createChanel", { channelType })}
            className="text-zinc-500 hover:text-zinc-600 dark:text-zinc-400 dark:hover:text-zinc-300 transition"
          >
            <Plus className="w-4 h-4" />
          </button>
        </ActionTooltip>
      )}
      {role === RoleAccount.ADMIN && sectionType === "members" && (
        <ActionTooltip label="Quản lý" side="top">
          <button
            onClick={() => onOpen("manageMember", { server })}
            className="text-zinc-500 hover:text-zinc-600 dark:text-zinc-400 dark:hover:text-zinc-300 transition"
          >
            <Settings className="w-4 h-4" />
          </button>
        </ActionTooltip>
      )}
    </div>
  );
};

export default ServerSection;
