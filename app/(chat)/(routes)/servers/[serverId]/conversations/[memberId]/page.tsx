import ChatHeader from "@/containers/chat/channel/chat/chat.header";
import { checkAuthTokenClient } from "@/lib/auth";
import { getOrCreateConversation } from "@/lib/conversation";
import { db } from "@/lib/prisma";
import { redirect } from "next/navigation";
import React from "react";

interface MemberIdPageProps {
  params: {
    memberId: string;
    serverId: string;
  };
}

const MemberIdPage = async ({ params }: MemberIdPageProps) => {
  const profile = await checkAuthTokenClient();

  if (!profile) {
    return redirect("/login");
  }

  const currenMember = await db.member.findFirst({
    where: {
      serverId: params.serverId,
      profileId: profile.id,
    },
    include: {
      profile: true,
    },
  });

  if (!currenMember) {
    return redirect("/chat");
  }

  const conversation = await getOrCreateConversation(
    currenMember.id,
    params.memberId
  );

  console.log({ conversation });

  if (!conversation) {
    return redirect(`/servers/${params.serverId}`);
  }

  const { memberOne, memberTwo } = conversation;

  const otherMember =
    memberOne.profileId === profile.id ? memberTwo : memberOne;
  return (
    <div>
      <ChatHeader
        imageUrl={otherMember.profile.avatar ?? undefined}
        name={otherMember.profile.name}
        serverId={params.serverId}
        type="conversation"
      />
    </div>
  );
};

export default MemberIdPage;
