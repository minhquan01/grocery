"use client";

import ButtonCustom from "@/components/button.custom";
import RenderInputFrom from "@/components/form/render.input";
import { useAuth } from "@/components/providers/auth.provider";
import { Form } from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";
import * as z from "zod";

const SignInContent = () => {
  const { login } = useAuth();
  const router = useRouter();

  const formSchema = z.object({
    username: z.string().min(1, {
      message: "Vui lòng nhập tên",
    }),
    password: z.string().min(1, {
      message: "Vui lòng nhập mật khẩu",
    }),
  });

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });
  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const res = await login(values);
  };

  const isLoading = form.formState.isSubmitting;

  return (
    <div className="md:min-w-[500px]">
      <div className="mb-5 text-center">
        <h1 className="text-lg md:text-2xl font-medium">
          Hellu lại gặp lại rồii 🤭
        </h1>
        <p className="text-[13px] md:text-sm mt-2 dark:text-white/70 text-black/70">
          Zô zô
        </p>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-5">
          <div className="space-y-4">
            {fieldLogin.map((item, idx) => (
              <RenderInputFrom
                form={form}
                label={item.label}
                name={item.name}
                isLoading={isLoading}
                type={item.type}
                key={idx}
              />
            ))}
          </div>

          <ButtonCustom type="submit" isLoading={isLoading} className="w-full">
            Đăng nhập
          </ButtonCustom>
          <div className="text-[15px] dark:text-white/60 text-black/60">
            Bạn chưa có tài khoản?{" "}
            <Link
              className="underline text-primary/80 hover:text-primary"
              href="/sign-up"
            >
              Đăng ký
            </Link>
          </div>
        </form>
      </Form>
    </div>
  );
};

export default SignInContent;

const fieldLogin = [
  {
    name: "username",
    label: "Tên đăng nhập",
  },
  {
    name: "password",
    label: "Mật khẩu",
    type: "password",
  },
];
