"use client";

import { useForm } from "react-hook-form";
import * as z from "zod";

import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
} from "@/components/ui/form";
import { useModal } from "@/hooks/use.modal.store";
import { zodResolver } from "@hookform/resolvers/zod";
import { useParams, useRouter } from "next/navigation";

import { ChannelType } from "@prisma/client";
import ButtonCustom from "../button.custom";
import RenderInputFrom from "../form/render.input";
import { InitModal } from "./init.modal";
import SelectCustom from "../select.custom";
import { useEffect, useMemo } from "react";
import { createChannel } from "@/services/chat";

const formSchema = z.object({
  name: z.string().min(1, {
    message: "Channel name is required.",
  }),
  type: z.nativeEnum(ChannelType),
});
const CreateChannelModal = () => {
  const { isOpen, onClose, type, data } = useModal();
  const router = useRouter();
  const params = useParams();

  const isModalOpen = isOpen && type === "createChanel";

  const { channelType } = data;

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      name: "",
      type: channelType || ChannelType.TEXT,
    },
  });

  useEffect(() => {
    if (channelType) {
      form.setValue("type", channelType);
    } else {
      form.setValue("type", ChannelType.TEXT);
    }
  }, [channelType, form]);

  const selectTypeChannel = useMemo(() => {
    return Object.values(ChannelType).map((type) => ({
      value: type,
      name: type.toLowerCase(),
    }));
  }, [ChannelType]);

  const isLoading = form.formState.isSubmitting;

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      const query = {
        serverId: String(params?.serverId),
      };
      const res = await createChannel(values, query);
      if (res.status === 200) {
        form.reset();
        onClose();
        router.refresh();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    form.reset();
    onClose();
  };

  return (
    <InitModal
      action={false}
      title="Tạo kênh"
      isModalOpen={isModalOpen}
      onClose={handleClose}
    >
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
          <div className="space-y-8 ">
            <RenderInputFrom form={form} label="Tên kênh" name="name" />
            <div className="w-full">
              <FormField
                control={form.control}
                name="type"
                render={({ field }) => {
                  return (
                    <FormItem>
                      <FormLabel className="text-base text-[#FFFFFFCC]">
                        Loại kênh
                      </FormLabel>
                      <SelectCustom
                        placeholder="Chọn kênh"
                        dataSelect={selectTypeChannel}
                        disabled={isLoading}
                        onValueChange={field.onChange}
                        defaultValue={field.value}
                      />
                    </FormItem>
                  );
                }}
              />
            </div>
          </div>
          <div className="flex items-center justify-between">
            <ButtonCustom
              className="w-full"
              isLoading={isLoading}
              type="submit"
            >
              Tạo
            </ButtonCustom>
          </div>
        </form>
      </Form>
    </InitModal>
  );
};

export default CreateChannelModal;
