"use client";

import { darkThemeToast, lightThemeToast } from "@/theme";
import { useTheme } from "next-themes";
import React from "react";
import { Toaster } from "react-hot-toast";

const ToastConfig = () => {
  const { theme } = useTheme();
  return (
    <Toaster
      toastOptions={theme === "dark" ? darkThemeToast : lightThemeToast}
    />
  );
};

export default ToastConfig;
