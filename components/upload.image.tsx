"use client";

import "@uploadthing/react/styles.css";
import { UploadDropzone } from "@/utils/uploadthing";
import Image from "next/image";
import { FileIcon, X } from "lucide-react";

interface UploadCoverBlogProps {
  onChange: (url?: string) => void;
  value: string;
  endpoint: "messageFile" | "serverImage";
}

const UploadImage = ({ onChange, value, endpoint }: UploadCoverBlogProps) => {
  const fileType = value?.split(".").pop();

  if (value && fileType !== "pdf") {
    return (
      <div className="relative h-20 w-20">
        <Image fill src={value} alt="Avatar" className="rounded-full" />
        {value && (
          <button
            onClick={() => onChange("")}
            className="bg-rose-500 text-white p-1 rounded-full absolute top-0 right-0 shadow-sm"
            type="button"
          >
            <X className="w-3 h-3" />
          </button>
        )}
      </div>
    );
  }
  if (value && fileType === "pdf") {
    return (
      <div className="relative flex items-center p-2 mt-2 rounded-md bg-white/5">
        <FileIcon className="h-10 w-10 fill-green-300 stroke-primary" />
        <a
          href={value}
          target="_blank"
          rel="noopener noreferrer"
          className="ml-2 text-sm text-primary hover:underline"
        >
          {value}
        </a>
        {value && (
          <button
            onClick={() => {
              onChange("");
            }}
            className="bg-rose-500 text-white p-1 rounded-full absolute -top-2 -right-1.5 shadow-sm"
            type="button"
          >
            <X className="w-3 h-3" />
          </button>
        )}
      </div>
    );
  }
  return (
    <div className="dark:border dark:rounded-2xl dark:border-white/10 dark:border-dashed cursor-pointer">
      <UploadDropzone
        className="uploadthing select-none"
        endpoint={endpoint}
        onClientUploadComplete={(res) => {
          onChange(res?.[0].fileUrl);
        }}
        onUploadError={(error: Error) => {
          console.log(error);
        }}
      />
    </div>
  );
};

export default UploadImage;
