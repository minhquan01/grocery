"use client";

import { authToken } from "@/constant";
import { LoginProps, newAccount } from "@/interfaces";
import { authApi } from "@/services/api";
import { useRouter } from "next/navigation";
import React, {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import toast from "react-hot-toast";
import Cookies from "js-cookie";

interface AuthContextProps {
  login: (values: LoginProps) => void;
  me: () => void;
  user: any;
  register: (values: newAccount) => void;
  logout: () => void;
}
interface AuthProviderProps {
  children: ReactNode;
}

const defaultProvider: AuthContextProps = {
  login: () => null,
  user: null,
  me: () => null,
  register: () => null,
  logout: () => null,
};

const AuthContext = createContext(defaultProvider);

const AuthProvider = ({ children }: AuthProviderProps) => {
  const getUser = (): any | null => {
    try {
      if (typeof window !== "undefined") {
        const user = localStorage.getItem("user");
        if (user) {
          return JSON.parse(user);
        }
      }
    } catch (error) {
      return null;
    }
  };
  const [user, setUser] = useState(getUser());
  const router = useRouter();

  const handleLogin = async (values: LoginProps) => {
    try {
      const authData = await authApi.loginApi(values);
      const data = authData.data;
      localStorage.setItem(authToken.accessToken, data.accessToken);
      localStorage.setItem(authToken.refreshToken, data.refreshToken);
      Cookies.set(authToken.accessToken, data.accessToken, { expires: 365 });
      handleFetchMe();
      router.push("/");
    } catch (error) {
      console.log({ error });
      console.error("Đăng nhập thất bại:", error);
    }
  };

  const handleRegister = async (values: newAccount) => {
    try {
      const result = await authApi.registerApi(values);
      if (result.data.status === 201) {
        router.push("/sign-in");
      }
    } catch (error) {
      toast.error("1");
      console.error("Đăng ký thất bại:", error);
    }
  };

  const handleFetchMe = async () => {
    try {
      const dataMe = await authApi.fetchMe();
      localStorage.setItem("user", JSON.stringify(dataMe.user));
      setUser(dataMe.user);
    } catch (error) {
      console.error("Xác thực thất bại", error);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem(authToken.accessToken);
    localStorage.removeItem(authToken.refreshToken);
    Cookies.remove(authToken.accessToken);
    setUser(null);
    localStorage.removeItem("user");
  };
  useEffect(() => {
    (async () => {
      await handleFetchMe();
    })();
  }, []);

  const value = {
    login: handleLogin,
    me: handleFetchMe,
    user: user,
    register: handleRegister,
    logout: handleLogout,
  };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthProvider;

export const useAuth = () => {
  const authContext = useContext(AuthContext);
  if (!authContext) {
    throw new Error("Forgot to wrap component in AuthContext");
  }
  return authContext;
};
