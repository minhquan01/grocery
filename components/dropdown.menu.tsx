"use client";

import { CreditCard, Keyboard, LogOut, Settings, User } from "lucide-react";

import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { useAuth } from "./providers/auth.provider";
import { ReactNode } from "react";
import Link from "next/link";
import { MdKeyboardArrowDown } from "react-icons/md";
import { DataItemDropDownProps } from "@/interfaces";
import { ModalType, useModal } from "@/hooks/use.modal.store";
import { RoleAccount } from "@prisma/client";

interface DropdownMenuCustomProps {
  children: ReactNode;
  title?: string;
  isLogout?: boolean;
  dataItem: DataItemDropDownProps[];
  iconDrop?: boolean;
  variant?: "default" | "between";
  capitalize?: boolean;
  role?: RoleAccount;
}

export function DropdownMenuCustom({
  children,
  title,
  isLogout = false,
  dataItem,
  iconDrop = false,
  variant = "default",
  capitalize = false,
  role,
}: DropdownMenuCustomProps) {
  const { logout, user } = useAuth();
  const { onOpen } = useModal();
  // const isAdmin = role === RoleAccount.ADMIN;
  // const isModerator = isAdmin || role === RoleAccount.EDITOR;

  const checkRole = (roleList: string[] | string | undefined) => {
    if (roleList === "ALL" || !roleList) {
      return true;
    } else if (roleList.includes(role ?? "")) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <div className="flex items-center justify-between">
          {children}{" "}
          {iconDrop && (
            <div>
              <MdKeyboardArrowDown />
            </div>
          )}
        </div>
      </DropdownMenuTrigger>
      <DropdownMenuContent className="w-56">
        {title && (
          <>
            <DropdownMenuLabel>{title}</DropdownMenuLabel>
            <DropdownMenuSeparator />
          </>
        )}
        <DropdownMenuGroup
          className={`font-medium ${capitalize && "capitalize"}`}
        >
          {dataItem.map((item, idx) => {
            const isAuthorization = checkRole(item.role ?? undefined);
            return (
              <>
                {isAuthorization && (
                  <DropdownMenuItem
                    onClick={() => {
                      if (!item.modal) return;
                      onOpen(item.modal as ModalType, item.dataModal);
                    }}
                    className={`cursor-pointer text-[#B5BAC1] ${
                      item.className ? item.className : "hover:text-white"
                    }`}
                    key={idx}
                  >
                    {item.url ? (
                      <Link
                        href={item.url}
                        className={`flex items-center w-full px-1 ${
                          variant === "between"
                            ? "justify-between flex-row-reverse"
                            : "gap-x-2"
                        } `}
                      >
                        {item.icon}
                        <span>{item.title}</span>
                      </Link>
                    ) : (
                      <div
                        className={`flex items-center w-full px-1 ${
                          variant === "between"
                            ? "justify-between flex-row-reverse"
                            : "gap-x-2"
                        } `}
                      >
                        {item.icon}
                        <span>{item.title}</span>
                      </div>
                    )}
                  </DropdownMenuItem>
                )}
              </>
            );
          })}
        </DropdownMenuGroup>
        {isLogout && (
          <>
            <DropdownMenuSeparator />
            <DropdownMenuItem
              className="flex items-center gap-x-2 text-[#B5BAC1] hover:text-white"
              onClick={logout}
            >
              <LogOut className="h-4 w-4" />
              <span>Đăng xuất</span>
            </DropdownMenuItem>
          </>
        )}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}
