import { db } from "@/lib/prisma";
import { NextRequest } from "next/server";
import ToggleMode from "../toggle.mode";
import { ScrollArea } from "../ui/scroll-area";
import { Separator } from "../ui/separator";
import HomeChat from "./home.chat";
import NavigationAction from "./navigation.action";
import NavigationItem from "./navigation.item";
import { cookies } from "next/headers";
import { checkAuthTokenClient } from "@/lib/auth";
import { redirect } from "next/navigation";

const NavigationSidebar = async () => {
  const profile = await checkAuthTokenClient();
  if (!profile) {
    redirect("/");
  }
  const servers = await db.server.findMany({
    where: {
      members: {
        some: {
          profileId: profile?.id,
        },
      },
    },
  });

  return (
    <div className="space-y-4 py-2 flex flex-col items-center h-full text-primary-green w-full border border-r-black/5 dark:border-none dark:bg-[#1F2937]">
      <HomeChat />
      <Separator className="h-0.5 bg-zinc-300 dark:bg-zinc-700 rounded-md w-10 mx-auto" />
      <ScrollArea className="flex-1 w-full">
        {servers.map((server) => (
          <div key={server.id} className="mb-4">
            <NavigationItem
              avatar={server.avatar}
              id={server.id}
              name={server.name}
            />
          </div>
        ))}
        <NavigationAction />
      </ScrollArea>
      <div className="pb-3 mt-auto flex items-center flex-col gap-y-4">
        <ToggleMode />
      </div>
    </div>
  );
};

export default NavigationSidebar;
