// auth.ts
import { NextApiRequest, NextApiResponse } from "next";
import jwt from "jsonwebtoken";
import { authToken, secretKey } from "@/constant";
import { NextRequest, NextResponse } from "next/server";
import { db } from "./prisma";
import { cookies, headers } from "next/headers";
import { redirect } from "next/navigation";

export async function checkAuthToken() {
  try {
    const headersList = headers();
    console.log({ headersList });
    const token = headersList.get("authorization");
    if (!token || !token.startsWith("Bearer ")) {
      return NextResponse.json(
        {
          message: "Unauthorized",
        },
        { status: 401 }
      );
    }

    const authTokenSlice = token.slice(7);
    const decoded: any = jwt.verify(authTokenSlice, secretKey);
    const user = await db.account.findUnique({
      where: {
        id: decoded.id,
      },
    });
    if (!user) {
      return false;
    }
    const profile = await db.profile.findUnique({
      where: {
        accountId: user.id,
      },
    });

    return profile as any;
  } catch (error) {
    return false;
  }
}

export async function checkAuthTokenClient() {
  try {
    const token = cookies().get(authToken.accessToken);
    if (!token) return false;
    const decoded: any = jwt.verify(token.value, secretKey);
    const user = await db.account.findUnique({
      where: {
        id: decoded.id,
      },
    });
    if (!user) {
      return false;
    }
    const profile = await db.profile.findUnique({
      where: {
        accountId: user.id,
      },
    });

    return profile as any;
  } catch (error) {
    return false;
  }
}

export async function checkAuthTokenPages(req: NextApiRequest) {
  try {
    const token = req.headers.authorization;
    if (!token || !token.startsWith("Bearer ")) {
      return NextResponse.json(
        {
          message: "Unauthorized",
        },
        { status: 401 }
      );
    }

    const authTokenSlice = token.slice(7);
    const decoded: any = jwt.verify(authTokenSlice, secretKey);
    const user = await db.account.findUnique({
      where: {
        id: decoded.id,
      },
    });
    if (!user) {
      return false;
    }
    const profile = await db.profile.findUnique({
      where: {
        accountId: user.id,
      },
    });

    return profile as any;
  } catch (error) {
    return false;
  }
}
