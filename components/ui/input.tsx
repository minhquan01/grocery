import * as React from "react";

import { cn } from "@/lib/utils";

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  variant?: "default" | "fill";
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, type, variant = "default", ...props }, ref) => {
    return (
      <input
        type={type}
        className={cn(
          "flex w-full py-2 px-2 md:py-3 md:px-2.5 max-md:text-sm rounded-xl md:rounded-2xl bg-transparent border dark:border-neutral-200/30 border-black/10",
          variant === "default" ? "" : "bg-[#EAE9EB] dark:bg-[#5c5b5b]/20",
          className
        )}
        ref={ref}
        {...props}
      />
    );
  }
);
Input.displayName = "Input";

export { Input };
