import { Metadata } from "next";

export const setMeta = ({
  title,
  description,
}: {
  title: string;
  description?: string;
}) => {
  const metadata: Metadata = {
    title: title,
    description: description ?? "Pure",
  };
  return metadata;
};
