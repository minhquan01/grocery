import React from "react";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

interface SelectCustomProps {
  placeholder?: string;
  label?: string;
  dataSelect: {
    value: string;
    name: string;
  }[];
  onValueChange: () => void;
  defaultValue: string;
  disabled: boolean;
}

const SelectCustom = ({
  placeholder,
  label,
  dataSelect,
  onValueChange,
  defaultValue,
  disabled,
}: SelectCustomProps) => {
  return (
    <Select
      defaultValue={defaultValue}
      disabled={disabled}
      onValueChange={onValueChange}
    >
      <SelectTrigger className="w-[180px]">
        <SelectValue placeholder={placeholder} />
      </SelectTrigger>
      <SelectContent>
        <SelectGroup>
          <SelectLabel>{label}</SelectLabel>
          {dataSelect.map((item, idx) => (
            <SelectItem className="capitalize" value={item.value} key={idx}>
              {item.name}
            </SelectItem>
          ))}
        </SelectGroup>
      </SelectContent>
    </Select>
  );
};

export default SelectCustom;
