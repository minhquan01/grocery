import React, { ReactNode } from "react";

const LayoutBlogDetail = ({ children }: { children: ReactNode }) => {
  return <div>{children}</div>;
};

export default LayoutBlogDetail;
