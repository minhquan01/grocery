import Content from "@/components/content";
import QuillEditor from "@/components/editor";
import ListBlog from "@/containers/blog/list.blog";
import React from "react";

const Blog = () => {
  return (
    <div>
      <ListBlog />
    </div>
  );
};

export default Blog;
