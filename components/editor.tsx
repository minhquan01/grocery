"use client";

import React, { useRef, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const QuillEditor: React.FC = () => {
  const quillRef = useRef<ReactQuill | null>(null);

  const modules = {
    toolbar: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike"],
      ["blockquote", "code-block"],
      [{ list: "ordered" }, { list: "bullet" }],
      [{ indent: "-1" }, { indent: "+1" }],
      [{ direction: "rtl" }],
      [{ color: [] }, { background: [] }],
      [{ align: [] }],
      ["link", "image", "video"],
    ],
  };

  const formats = [
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "color",
    "background",
    "script",
    "header",
    "blockquote",
    "code-block",
    "indent",
    "list",
    "direction",
    "align",
    "link",
    "image",
    "video",
    "formula",
  ];

  const [editorHtml, setEditorHtml] = useState("");

  return (
    <div>
      <ReactQuill
        ref={(el) => {
          if (el) quillRef.current = el;
        }}
        theme="snow"
        modules={modules}
        formats={formats}
        onChange={(e) => {
          setEditorHtml(e);
        }}
        className="placeholder:text-white"
        placeholder="Nội dung bài viết"
        value={editorHtml}
      />
    </div>
  );
};

export default QuillEditor;
