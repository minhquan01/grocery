import { axiosCustom } from "./axios.cutom";

const messageApi = {
  createMessage: async (
    apiUrl: string,
    values: { content: string },
    query: Record<string, any>
  ) => {
    return axiosCustom.post(`${apiUrl}`, values, { params: query });
  },
};

export default messageApi;
