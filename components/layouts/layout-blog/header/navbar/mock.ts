export const listNav = [
  {
    name: "Trang chủ",
    url: "/",
  },
  {
    name: "Blog",
    url: "/blog",
  },
  {
    name: "Chat",
    url: "/chat",
  },
  {
    name: "Me",
    url: "/me",
  },
];
