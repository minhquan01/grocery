import { socketUrl } from "@/constant";
import ChatHeader from "@/containers/chat/channel/chat/chat.header";
import ChatInput from "@/containers/chat/chat.input";
import ChatMessages from "@/containers/chat/chat.messages";
import { checkAuthTokenClient } from "@/lib/auth";
import { db } from "@/lib/prisma";
import { redirect } from "next/navigation";
import React from "react";

interface ChannelIdPageProps {
  params: {
    serverId: string;
    channelId: string;
  };
}

const ChannelIdPage = async ({ params }: ChannelIdPageProps) => {
  const profile = await checkAuthTokenClient();

  if (!profile) {
    return redirect("/login");
  }

  const channel = await db.channel.findUnique({
    where: {
      id: params.channelId,
    },
  });

  const member = await db.member.findFirst({
    where: {
      serverId: params.serverId,
      profileId: profile.id,
    },
  });

  if (!channel || !member) {
    redirect("/chat");
  }

  return (
    <div className=" flex flex-col min-h-screen h-full">
      <ChatHeader
        name={channel.name}
        serverId={channel.serverId}
        type="channel"
      />
      <ChatMessages
        member={member}
        name={channel.name}
        type="channel"
        apiUrl="/messages"
        socketUrl={socketUrl}
        socketQuery={{
          channelId: channel.id,
          serverId: channel.serverId,
        }}
        paramKey="channelId"
        paramValue={channel.id}
        chatId={channel.id}
      />
      <ChatInput
        name={channel.name}
        type="channel"
        apiUrl="/socket/messages"
        query={{
          channelId: channel.id,
          serverId: channel.serverId,
        }}
      />
    </div>
  );
};

export default ChannelIdPage;
