"use client";

import BoxGlass from "@/components/box.glass";
import ToggleMode from "@/components/toggle.mode";
import React, { ReactNode } from "react";

interface AuthLayoutProps {
  children: ReactNode;
}

const AuthLayout = ({ children }: AuthLayoutProps) => {
  return (
    <div className="h-screen">
      <div className="fixed top-5 right-5 md:right-10 z-50">
        <ToggleMode />
      </div>
      <div className="relative flex flex-col gap-y-10 md:gap-y-14 items-center pt-20 md:pt-28">
        <div className="bg-primary/20 md:bg-primary/20 absolute w-20 h-20 md:w-40 md:h-40 rounded-full blur-2xl md:left-[26%] top-28 left-[10%] md:top-[20%]" />
        <div className="bg-primary/20 md:bg-primary/20 absolute w-20 h-20 md:w-40 md:h-40 rounded-full blur-2xl right-7 bottom-[1%] md:right-[30%] md:bottom-1" />
        <h1 className="text-primary text-3xl md:text-5xl font-bold">Pure</h1>
        <div className="max-md:w-mobile mx-auto">
          <BoxGlass>{children}</BoxGlass>
        </div>
      </div>
    </div>
  );
};

export default AuthLayout;
