import { useCopy } from "@/hooks/use.copy";
import { useModal } from "@/hooks/use.modal.store";
import { useState } from "react";
import ButtonCustom from "../button.custom";
import { InitModal } from "./init.modal";
import { deleteChannel, deleteServer, leaveServer } from "@/services/chat";
import { useParams, useRouter } from "next/navigation";

const DeleteChannelModal = () => {
  const [isLoading, setIsLoading] = useState(false);

  const { isOpen, onClose, type, data } = useModal();

  const router = useRouter();
  const isModalOpen = isOpen && type === "deleteChannel";
  const { server, channel } = data;

  const handleLeave = async () => {
    try {
      if (!channel?.id) return;
      const query = {
        serverId: String(server?.id),
      };
      setIsLoading(true);
      const res = await deleteChannel(channel?.id, query);
      if (res.status === 200) {
        router.refresh();
        onClose();
        router.push(`/servers/${server?.id}`);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <InitModal
      action={false}
      title="Xoá kênh"
      isModalOpen={isModalOpen}
      description={
        <div className="text-zinc-500">
          Bạn muốn xoá nó thật à? <br />
          <span className="font-bold text-primary">#{channel?.name}</span> sẽ bị
          xoá vĩnh viễn.
        </div>
      }
      onClose={onClose}
    >
      <div className="flex items-center justify-around mt-8">
        <ButtonCustom
          className="bg-red-500 w-full"
          onClick={handleLeave}
          isLoading={isLoading}
          type="submit"
        >
          Umm
        </ButtonCustom>
      </div>
    </InitModal>
  );
};

export default DeleteChannelModal;
