"use client";

import React, { useEffect, useState } from "react";
import Action from "./action";
import NavbarBlog from "./navbar";

const HeaderBlog = () => {
  const [prevSrollY, setPrevSrollY] = useState(0);
  const [isHiddenHeader, setIsHiddenHeader] = useState(true);
  const [directionScroll, setDirectionScroll] = useState<"up" | "down">("up");

  useEffect(() => {
    const handleScroll = () => {
      setPrevSrollY(window.scrollY);
      const scrollY = window.scrollY;
      if (scrollY > 10) {
        setIsHiddenHeader(true);
      } else {
        setIsHiddenHeader(false);
      }
      if (scrollY < prevSrollY) {
        setDirectionScroll("up");
      } else {
        setDirectionScroll("down");
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [prevSrollY]);

  return (
    <header
      className={`sticky w-full right-0 left-0 transition-all duration-200 z-[999] ${
        isHiddenHeader && directionScroll === "down"
          ? "-top-[81px]"
          : "top-[0px]"
      }`}
    >
      <div className="relative bg-white shadow-md dark:bg-dark-background py-1.5 md:py-4 border-b dark:border-white/10 border-black/10">
        <div className="max-w-mobile md:max-w-main mx-auto h-10 md:h-12 flex items-center justify-between">
          <div className="flex items-center gap-6">
            <h1 className="text-primary text-xl md:text-4xl font-extrabold">
              Pure
            </h1>
            <NavbarBlog />
          </div>
          <Action />
        </div>
      </div>
    </header>
  );
};

export default HeaderBlog;
