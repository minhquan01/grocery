import React from "react";

function Icon() {
  return (
    <svg width="24" height="24" fill="currentColor" viewBox="0 0 24 24">
      <path
        fillRule="evenodd"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M11.995 7.233c-1.45-1.623-3.867-2.06-5.683-.573-1.816 1.486-2.072 3.971-.645 5.73l6.328 5.86 6.329-5.86c1.426-1.759 1.201-4.26-.646-5.73-1.848-1.471-4.233-1.05-5.683.573z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Icon;
