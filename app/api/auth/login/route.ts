import { db } from "@/lib/prisma";
import bcrypt from "bcrypt";
import { NextResponse } from "next/server";
import jwt from "jsonwebtoken";
import { secretKey } from "@/constant";
import { NextApiResponse } from "next";

export async function POST(req: Request, res: NextApiResponse) {
  try {
    const body = await req.json();
    const { username, password } = body;
    const existAccount = await db.account.findFirst({
      where: {
        username: username,
      },
    });
    if (!existAccount) {
      return NextResponse.json(
        {
          message: "Tên đăng nhập không đúng",
        },
        { status: 404 }
      );
    }
    const validPass = await bcrypt.compare(password, existAccount.password);
    if (!validPass) {
      return NextResponse.json(
        {
          message: "Mật khẩu không chính xác",
        },
        { status: 404 }
      );
    }

    const profile = await db.profile.findUnique({
      where: {
        accountId: existAccount.id,
      },
    });

    if (!profile) {
      return NextResponse.json(
        {
          message: "Không tìm thấy profile",
        },
        { status: 404 }
      );
    }

    const accessToken = jwt.sign(
      {
        id: existAccount.id,
        username: existAccount.username,
        role: existAccount.role,
        profileId: profile.id,
      },
      secretKey,
      { expiresIn: "24h" }
    );
    const refreshToken = jwt.sign(
      {
        id: existAccount.id,
        username: existAccount.username,
        role: existAccount.role,
        profileId: profile.id,
      },
      secretKey,
      { expiresIn: "7d" }
    );
    return NextResponse.json({
      accessToken,
      refreshToken,
      message: "Xác nhận thành công",
      status: 200,
    });
  } catch (error) {
    throw NextResponse.json({
      error,
      status: 500,
    });
  }
}
