import { ModalData } from "@/hooks/use.modal.store";
import { ChannelType } from "@prisma/client";

export interface newAccount {
  username: string;
  name: string;
  password: string;
}

export interface LoginProps {
  username: string;
  password: string;
}

export interface CreateServerProps {
  name: string;
  avatar: string;
}

export interface DataItemDropDownProps {
  url?: string;
  modal?: string;
  icon: React.JSX.Element;
  title: string;
  className?: string;
  role?: string | string[];
  dataModal?: ModalData;
}

export interface DataCreateChannelProps {
  name: string;
  type: ChannelType;
}
