// Icon
export { default as IconMoon } from "./icons/moon";
export { default as IconSun } from "./icons/sun";
export { default as IconSearch } from "./icons/search";
export { default as IconHome } from "./icons/home";
export { default as IconMenu } from "./icons/menu";
export { default as IconSearchMobile } from "./icons/search.mobile";
export { default as IconClose } from "./icons/close";
export { default as IconFacebook } from "./icons/social/facebook";
export { default as IconLinkedin } from "./icons/social/linkedin";
export { default as IconYoutube } from "./icons/social/youtube";
export { default as IconTiktok } from "./icons/social/tiktok";
export { default as IconGitlab } from "./icons/social/gitlab";
export { default as IconMail } from "./icons/social/mail";
export { default as IconEye } from "./icons/eye";
export { default as IconComment } from "./icons/comment";
export { default as IconHeartLight } from "./icons/heart.light";
export { default as IconHeartBold } from "./icons/heart.bold";
export { default as IconUpload } from "./icons/upload";

// Image
export { default as AvatarImage } from "./images/avatar.jpg";
