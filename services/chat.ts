import { CreateServerProps, DataCreateChannelProps } from "@/interfaces";
import { axiosCustom } from "./axios.cutom";
import { Server } from "@prisma/client";

export async function getAllServer() {
  const res = await axiosCustom.get("/server");
  return res;
}

export async function createServerChat(values: CreateServerProps) {
  const res = await axiosCustom.post("/server", values);
  return res;
}

export async function editServerChat(
  values: CreateServerProps,
  server: Server
) {
  const res = await axiosCustom.patch(`/server/${server?.id}`, values);
  return res;
}

export async function editRoleMember(
  memberId: string,
  role: string,
  params: { serverId: string }
) {
  const res = await axiosCustom.patch(
    `/member/${memberId}`,
    { role },
    { params: params }
  );
  return res;
}

export async function kickMember(
  memberId: string,
  params: { serverId: string }
) {
  const res = await axiosCustom.delete(`/member/${memberId}`, {
    params: params,
  });
  return res;
}

export async function createChannel(
  data: DataCreateChannelProps,
  params: { serverId: string }
) {
  return axiosCustom.post("/channels", data, { params: params });
}

export async function leaveServer(id: string) {
  return axiosCustom.patch(`/server/${id}/leave`);
}

export async function deleteServer(id: string) {
  return axiosCustom.delete(`/server/${id}`);
}

export async function deleteChannel(id: string, params: { serverId: string }) {
  return axiosCustom.delete(`/channels/${id}`, { params: params });
}

export async function editChannel(
  id: string,
  params: { serverId: string },
  data: DataCreateChannelProps
) {
  return axiosCustom.patch(`/channels/${id}`, data, { params: params });
}
