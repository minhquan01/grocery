"use client";

import ButtonCustom from "@/components/button.custom";
import { ComboboxPick } from "@/components/combobox.pick";
import QuillEditor from "@/components/editor";
import InputPick from "@/components/input.pick";
import { Form, FormControl, FormField, FormItem } from "@/components/ui/form";
import UploadImage from "@/components/upload.image";
import UploadCoverBlog from "@/components/upload.image";
import { zodResolver } from "@hookform/resolvers/zod";
import { X } from "lucide-react";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const CreateBlog = () => {
  const [valueCate, setValueCate] = useState("");
  const [listCategory, setListCategory] = useState<string[]>([]);
  const [imagePreview, setImagePreview] = useState<string | null>(null);

  const formSchema = z.object({
    title: z.string().min(4, { message: "Tiêu đề không nên quá ngắn" }),
  });

  const form = useForm({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      coverImage: "",
      category: [],
    },
  });
  useEffect(() => {
    setImagePreview(form.watch("coverImage"));
  }, [form.watch("coverImage")]);

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      // @ts-ignore
      form.setValue("category", listCategory);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteCategory = (value: string) => {
    if (!listCategory.includes(value)) {
      return;
    } else {
      setListCategory((prev) => prev.filter((item) => item !== value));
    }
  };

  useEffect(() => {
    if (!listCategory.includes(valueCate) && valueCate.length > 0) {
      setListCategory((prev) => [...prev, valueCate]);
    } else {
      return;
    }
  }, [valueCate]);

  return (
    <Form {...form}>
      <h3 className="mb-10 text-xl">Tạo bài viết</h3>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="border space-y-5 border-black/20 dark:bg-[#212A36] px-6 py-8 rounded-xl"
      >
        <FormField
          control={form.control}
          name="title"
          render={({ field }) => {
            return (
              <FormItem>
                <FormControl>
                  <input
                    type="text"
                    value={field.value}
                    onChange={field.onChange}
                    placeholder="Tiêu đề"
                    className="text-[36px] leading-[60px] placeholder:leading-[60px] py-3 outline-none w-full  bg-transparent"
                  />
                </FormControl>
              </FormItem>
            );
          }}
        />

        <div>
          <p className="mb-3">Nội dung</p>
          <div className="border rounded-2xl dark:border-white/10 border-dashed">
            <QuillEditor />
          </div>
        </div>
        <div>
          <p className="mb-3">Cover</p>
          {imagePreview ? (
            <div className="flex items-center justify-center">
              <Image
                width={1280}
                height={720}
                className="w-full m-0 rounded-xl overflow-hidden my-10 object-cover"
                sizes="(max-width: 1280px) 100vw, 1280px"
                data-lazy-sizes="(max-width: 1280px) 100vw, 1280px"
                src={imagePreview}
                alt="image cover"
              />
            </div>
          ) : (
            <FormField
              control={form.control}
              name="coverImage"
              render={({ field }) => (
                <FormItem>
                  <FormControl>
                    <UploadImage
                      endpoint="serverImage"
                      value={field.value}
                      onChange={field.onChange}
                    />
                  </FormControl>
                </FormItem>
              )}
            />
          )}
        </div>
        <div>
          <p className="mb-3">Chủ đề</p>
          <div className="flex items-center gap-x-3">
            <div className="flex items-center gap-x-2">
              {listCategory.map((item, idx) => (
                <div
                  key={idx}
                  className="group relative flex items-center gap-x-1 text-green-800 text-xs bg-green-100 cursor-pointer font-medium capitalize w-fit px-3 transition-colors  duration-300  rounded-full py-1"
                >
                  <p>{item}</p>
                  <div
                    onClick={() => handleDeleteCategory(item)}
                    className="text-green hover:text-red-500 px-1 cursor-pointer"
                  >
                    <X size={14} />
                  </div>
                </div>
              ))}
            </div>
            <ComboboxPick
              title="Chọn chủ đề"
              setValue={setValueCate}
              value={valueCate}
            />
          </div>
        </div>
        <div>
          <p className="mb-3">Tag</p>
          <InputPick />
        </div>
        <div className="flex items-center justify-end">
          <ButtonCustom type="submit">Tạo</ButtonCustom>
        </div>
      </form>
    </Form>
  );
};

export default CreateBlog;
