import React from "react";
import { Input, InputProps } from "./ui/input";

interface InputPickProps extends InputProps {}

const InputPick = ({ ...rest }: InputPickProps) => {
  return <Input {...rest} />;
};

export default InputPick;
