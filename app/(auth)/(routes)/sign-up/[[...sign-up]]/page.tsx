import BoxGlass from "@/components/box.glass";
import SignUpContent from "@/containers/auth/sign.up";
import React from "react";

const SignUp = () => {
  return <SignUpContent />;
};

export default SignUp;
