import React from "react";
import { Input, InputProps } from "./ui/input";
import { FormLabel } from "./ui/form";
import { Label } from "./ui/label";
import { Check, Copy } from "lucide-react";

interface InputCustomProps
  extends React.InputHTMLAttributes<HTMLInputElement>,
    InputProps {
  label?: string;
  id?: string;
  copy?: boolean;
  onCopyCustom?: () => void;
  copied?: boolean;
  loading?: boolean;
}

const InputCustom = ({
  id,
  label,
  copy = false,
  onCopyCustom,
  loading = false,
  copied = false,
  ...props
}: InputCustomProps) => {
  return (
    <div>
      {label && (
        <Label
          htmlFor={id}
          className="text-sm md:text-base font-medium text-zinc-500 dark:text-white/80 mb-1 block"
        >
          {label}
        </Label>
      )}
      <div className={`flex items-center gap-x-4 ${loading && "opacity-25"}`}>
        <Input id={id} {...props} autoComplete="off" />

        {copy && onCopyCustom && (
          <>
            {copied ? (
              <Check className="cursor-pointer" />
            ) : (
              <Copy onClick={() => onCopyCustom()} className="cursor-pointer" />
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default InputCustom;
